#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *
from crontab import CronTab

def addJobDaily(command, comment):
    """
    Add a daily cron task
    """
    # init cron
    # cron = CronTab(tab="""* * * * * """ + command + """ #""" + comment)
    cron = CronTab()
    cron_job = cron.new(command=command, comment=comment)
    cron_job.minute.every(2)
    # cron_job.minute.on(0)
    # cron_job.hour.on(0)

    # cron_job.enable()
    cron_job.enable(False)

    cron.write('logs.tab')
    if cron.render():
        # list = cron.find_comment('test2')
        # for value in list:
        #     current_app.logger.info(value)
        watprint(cron.render())
        # cron.remove(cron_job)
        return True
    else:
        # cron.remove(cron_job)
        return False

@app.route('/schedule/test', methods=['POST'])
def scheduleTest():

    # init cron
    # cron = CronTab()
    # watprint(cron)

    # add new cron job
    # curl -d '{"key1":"value1", "key2":"value2"}' -H "Content-Type: application/json" -X POST http://localhost:3000/schedule/updateDB
    # curl -d '{"key1": "value1", "key2": "value2"}' -H "Content-Type: application/json" -X POST http://localhost:3000/schedule/updateDB
    # job = cron.new(command='/usr/bin/echo', comment='U16180001')

    # job settings
    # job.hour.every(4)

    # addJobDaily('/usr/bin/curl -H "Content-Type: application/json" -X POST -d '{"key1": "value1"}' http://203.154.58.151:5000/schedule/updateDB', 'test')
    addJobDaily('/usr/bin/curl -H "Content-Type: application/json" -X POST -d {"key1": "value1"} http://203.154.58.151:5000/schedule/updateDB', 'test')
    return 'success'

@app.route('/schedule/updateDB', methods=['POST'])
def schedulerUpdateDB():
    watprint('test')
    watprint(request.json)
    # cron = CronTab()
    # return cron.find_comment('test')
    return 'updated'
