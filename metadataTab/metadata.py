#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *
import datetime

@app.route('/saveDataConfig', methods=['POST'])
def saveDataConfig():
    # expect:
    # { user: {email, firstname, lastname},
    #   source_id, source_name, download_type, status, webaddr,
    #   localpath, ref, schedule_mode, schedule_start, auto_approve, auto_generate, create_at }
    dataInput = request.json

    # watprint(dataInput)
    '''
    # watprint(dataInput)
    {
        u'status': u'1',
        u'source_name': u'test',
        u'schedule_start': u'1970-01-01 00:00:00',
        u'localpath': u'test.xml',
        u'append_mode': u'1', <<<<<
        u'webaddr': u'',
        u'schedule_mode': u'1',
        u'header_file': u'test.xml', <<<<<
        u'user': {
            u'lastname': u'Chimdee',
            u'email': u'chinnawat.ch@inet.co.th',
            u'firstname': u'Chinnawat'
        },
        u'source_id': 180017,
        u'download_type': u'5',
        u'ref': u'',
        u'auto_approve': u'1',
        u'auto_generate': u'0'
    }
    '''
    user = dataInput['user']

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email=%s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0 :
        if (user['firstname'] == result[0]['firstname'] and user['lastname'] == result[0]['lastname']):
            if dataInput['schedule_start'] != "":
                formatter = '%Y-%m-%d %H:%M:%S'
                tsStart = dataInput['schedule_start']
                scheduleStart = str(datetime.datetime.strptime(tsStart, formatter))
            else:
                scheduleStart = ""

            for idx, value in enumerate(dataInput):
                if dataInput[value] == None:
                    dataInput[value] = ''

            sql = "SELECT * FROM metadataTable WHERE source_id = %s and user_id = %s"
            cursor.execute(sql,(dataInput['source_id'], result[0]['user_id']))
            checkSrcID = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            resultCheckSrcID = toJson(checkSrcID,columns)
            pendingStatus = '2'
            logMsg = ''
            transDate = str(datetime.datetime.now(TZ).strftime("%Y-%m-%d %H:%M:%S"))
            if len(resultCheckSrcID) == 0:
                sql = "INSERT INTO metadataTable VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, NULL)"
                cursor.execute(sql, (result[0]['user_id'], dataInput['source_id'],
                                    dataInput['source_name'], dataInput['download_type'],
                                    dataInput['status'], dataInput['webaddr'],
                                    dataInput['localpath'], dataInput['header_file'],
                                    dataInput['append_mode'], dataInput['ref'],
                                    dataInput['schedule_mode'], scheduleStart,
                                    dataInput['auto_approve'], dataInput['auto_generate'],
                                    pendingStatus, logMsg, transDate))
            else:
                sql = "UPDATE metadataTable SET"
                sql += " source_name = '" + str(dataInput['source_name']).encode('string_escape')
                sql += "', download_type = '" + dataInput['download_type']
                sql += "', status = " + dataInput['status']
                sql += ", webaddr = '" + str(dataInput['webaddr']).encode('string_escape')
                sql += "', localpath = '" + str(dataInput['localpath']).encode('string_escape')
                sql += "', header_file = '" + str(dataInput['header_file']).encode('string_escape')
                sql += "', append_mode = '" + str(dataInput['append_mode']).encode('string_escape')
                sql += "', ref = '" + str(dataInput['ref']).encode('string_escape')
                sql += "', schedule_mode = '" + dataInput['schedule_mode']
                sql += "', schedule_start = '" + scheduleStart
                sql += "', auto_approve = '" + dataInput['auto_approve']
                sql += "', auto_generate = '" + dataInput['auto_generate']
                sql += "', import_status = '" + pendingStatus
                sql += "', trans_date = '" + transDate
                sql += "' WHERE source_id = '" + str(dataInput['source_id'])
                sql += "' and user_id = '" + str(result[0]['user_id']) + "'"
                cursor.execute(sql)
            conn.commit()
            cursor.close()
            return jsonify({"status": "success"})
        else:
            return jsonify({"status": "invalid user"})
    else:
        return jsonify({"status": "not found user"})

@app.route('/getDataConfig', methods=['POST'])
def getDataConfig():
    dataInput = request.json
    user = dataInput['user']

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email = %s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0 :
        if (user['firstname'] == result[0]['firstname'] and user['lastname'] == result[0]['lastname']):
            sql = "SELECT * FROM metadataTable WHERE user_id = %s"
            cursor.execute(sql,(result[0]['user_id']))
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            resultDataConfig = toJson(data,columns)
            conn.commit()
            cursor.close()

            for idx, data in enumerate(resultDataConfig):
                if resultDataConfig[idx]['schedule_start'] == None:
                    resultDataConfig[idx]['schedule_start'] = str(datetime.datetime.now(TZ).strftime("%Y-%m-%d"))

            if len(data) != 0:
                return jsonify(resultDataConfig)
            else:
                return jsonify({"status": "not found data"})
        else:
            return jsonify({"status": "invalid user"})
    else:
        return jsonify({"status": "not found user"})

@app.route('/deleteDataConfig', methods=['POST'])
def deleteDataConfig():
    dataInput = request.json
    user = dataInput['user']
    sid = str(dataInput['source_id'])

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email = %s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0 :
        if (user['firstname'] == result[0]['firstname'] and user['lastname'] == result[0]['lastname']):
            uid = str(result[0]['user_id'])
            try:
                sql = "DELETE FROM metadataTable WHERE source_id = %s and user_id = %s"
                cursor.execute(sql, (sid, result[0]['user_id']))
                data = cursor.fetchall()
                conn.commit()
                cursor.close()

                conn2 = mysqlDB.get_db()
                cursor2 = conn2.cursor()
                try:
                    sql2 = "USE U" + str(uid)
                    cursor2.execute(sql2)
                except Exception as e:
                    return jsonify({"result": "Not found user's database"})
                pre_sql = "SELECT concat('DROP TABLE IF EXISTS ', group_concat(table_name)) AS PRESQL FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'U" + uid + "' AND TABLE_NAME LIKE '" + sid + "%'"
                cursor2.execute(pre_sql)
                pre_data = cursor2.fetchall()
                columns = [column[0] for column in cursor2.description]
                sql2 = toJson(pre_data, columns)[0]['PRESQL']
                cursor2.execute(sql2)
                conn2.commit()
                cursor2.close()

                finderByFID = uid + sid
                fs_files = fs.find({ 'fid': {"$regex" : ".*"+finderByFID+".*"} })
                for value in fs_files:
                    fs.delete(value._id)
                fileInServer = 'uploads/' + uid + '/U' + uid + '_' + sid + '.*'
                for filename in glob.glob(fileInServer):
                    os.remove(filename)

                return jsonify({"status": "success"})
            except Exception as e:
                current_app.logger.info(str(e))
                return jsonify({"status": "invalid, " + str(e)})
        else:
            return jsonify({"status": "invalid user"})
    else:
        return jsonify({"status": "not found user"})

@app.route('/getLastSourceID', methods=['POST'])
def getLastSourceID():
    dataInput = request.json
    user = dataInput['user']

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email = %s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0 :
        if (user['firstname'] == result[0]['firstname'] and user['lastname'] == result[0]['lastname']):
            sql = "SELECT source_id FROM metadataTable WHERE user_id = %s ORDER BY source_id DESC LIMIT 1"
            cursor.execute(sql,(result[0]['user_id']))
            dataSrcID = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            resultSrcID = toJson(dataSrcID,columns)
            conn.commit()
            cursor.close()
            if resultSrcID:
                return jsonify(resultSrcID[0])
            else:
                return jsonify({"status": "not found source_id"})
        else:
            return jsonify({"status": "invalid user"})
    else:
        return jsonify({"status": "not found user"})

# def validateUser (user):
#     conn = mysql.connect()
#     cursor = conn.cursor()
#     sql = "SELECT user_id, firstname, lastname FROM user WHERE email=%s"
#     cursor.execute(sql,(user['email']))
#     data = cursor.fetchall()
#     columns = [column[0] for column in cursor.description]
#     result = toJson(data,columns)
#
#     return len(result)
