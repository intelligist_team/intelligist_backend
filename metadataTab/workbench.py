#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *
from dbConfig import mongoManage
from dbConfig import mssqlManage
from dbConfig import mysqlManage
from StringIO import StringIO
import xlrd
from collections import OrderedDict

try:
    import xml.etree.cElementTree as xmlet
except ImportError:
    import xml.etree.ElementTree as xmlet

@app.route('/getDataWorkbench', methods=['POST'])
def getDataWorkbench():
    dataInput = request.json
    user = dataInput['user']
    srcNameSearch = dataInput['source_name']

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email = %s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0 :
        if (user['firstname'] == result[0]['firstname'] and user['lastname'] == result[0]['lastname']):
            if srcNameSearch != "":
                sql = "SELECT source_id, source_name, import_status, log_message, trans_date FROM metadataTable WHERE user_id = %s and source_name = %s"
                cursor.execute(sql, (result[0]['user_id'], srcNameSearch))
            else:
                sql = "SELECT source_id, source_name, import_status, log_message, trans_date FROM metadataTable WHERE user_id = %s"
                cursor.execute(sql, (result[0]['user_id']))
            dataWB = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            resultDataWorkbench = toJson(dataWB,columns)
            conn.commit()
            cursor.close()

            if len(data) != 0:
                return jsonify(resultDataWorkbench)
            else:
                return jsonify({"status": "not found data"})
        else:
            return jsonify({"status": "invalid user"})
    else:
        return jsonify({"status": "not found user"})

@app.route('/downloadBySourceID', methods=['POST'])
def downloadBySourceID():
    dataInput = request.json
    user = dataInput['user']
    srcID = str(dataInput['source_id'])

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email = %s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)

    if len(result) != 0 :
        if (user['firstname'] == result[0]['firstname'] and user['lastname'] == result[0]['lastname']):

            # IDEA: Execute some function to read and show data in Table Style to frontend

            uid = str(result[0]['user_id'])

            sql = "SELECT webaddr, localpath, header_file, append_mode, download_type FROM metadataTable WHERE user_id = %s and source_id = %s"
            cursor.execute(sql, (uid, srcID))
            filePathData = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            resultPathData = toJson(filePathData, columns)

            # web mode: download_type = 1, 6
            if resultPathData[0]['download_type'] == '1' or resultPathData[0]['download_type'] == '6':
                current_app.logger.info('get data from address: ' + resultPathData[0]['webaddr'])
                cursor.close()
                return jsonify({"status": "failed", "description": "Sorry, System is Maintaining"})
            else: # file mode: download_type = 2, 3, 4, 5 as a file

                # header_file, append_mode

                if resultPathData[0]['header_file'] != '':
                    fileExtension = getFileExtension(resultPathData[0]['header_file'])
                    headerFileNameInServer = 'U' + uid + '_' + srcID + '_Header.' + fileExtension
                    resultImport = mongoManage.importContent(uid, srcID, headerFileNameInServer)

                    fileExtension = getFileExtension(resultPathData[0]['localpath'])
                    fileNameInServer = 'U' + uid + '_' + srcID + '.' + fileExtension
                    resultImport = mongoManage.importContent(uid, srcID, fileNameInServer)

                    resultImportMySQL = mysqlManage.insertMySQL(uid, srcID, resultPathData[0]['append_mode'], fileNameInServer, headerFileNameInServer)
                    # resultImportMySQL = mssqlManage.insertMsSQL(uid, srcID, resultPathData[0]['append_mode'], fileNameInServer, headerFileNameInServer)
                    # current_app.logger.info(resultImportMySQL)
                else:
                    fileExtension = getFileExtension(resultPathData[0]['localpath'])
                    fileNameInServer = 'U' + uid + '_' + srcID + '.' + fileExtension
                    resultImport = mongoManage.importContent(uid, srcID, fileNameInServer)
                    resultImportMySQL = mysqlManage.insertMySQL(uid, srcID, resultPathData[0]['append_mode'], fileNameInServer, '')
                    # resultImportMySQL = mssqlManage.insertMsSQL(uid, srcID, resultPathData[0]['append_mode'], fileNameInServer, '')
                    # current_app.logger.info(resultImportMySQL)

                # watprint(resultImportMySQL)
                if resultImportMySQL == 'success':
                    importStatus = '3'
                    logMsg = ''
                    # logMsg = 'Process Completed'
                    transDate = str(datetime.now(TZ).strftime("%Y-%m-%d %H:%M:%S"))
                    sql = "UPDATE metadataTable SET import_status = %s, log_message = %s, trans_date = %s WHERE user_id = %s and source_id = %s"
                    cursor.execute(sql, (importStatus, logMsg, transDate, uid, srcID))
                    conn.commit()
                    cursor.close()
                    return jsonify({"status": "success"})
                else:
                    importStatus = '1'
                    logMsg = resultImportMySQL
                    transDate = str(datetime.now(TZ).strftime("%Y-%m-%d %H:%M:%S"))
                    sql = "UPDATE metadataTable SET import_status = %s, log_message = %s, trans_date = %s WHERE user_id = %s and source_id = %s"
                    cursor.execute(sql, (importStatus, logMsg, transDate, str(uid), srcID))
                    conn.commit()
                    cursor.close()
                    return jsonify({"status": "failed", "description": resultImportMySQL})

            # WORKING
            # importStatus = '3'
            # logMsg = 'Process Completed'
            # transDate = str(datetime.now(TZ).strftime("%Y-%m-%d %H:%M:%S"))
            # sql = "UPDATE metadataTable SET import_status = %s, log_message = %s, trans_date = %s WHERE user_id = %s and source_id = %s"
            # cursor.execute(sql, (importStatus, logMsg, transDate, uid, srcID))

            # return jsonify({"status": "success"})
        else:
            cursor.close()
            return jsonify({"status": "failed", "description": "invalid user"})
    else:
        cursor.close()
        return jsonify({"status": "failed", "description": "not found user"})

# View Data Button
@app.route('/getDataTable', methods=['POST'])
def getDataTable():
    dataInput = request.json
    uid = str(getUserID(dataInput['user']))
    sid = str(dataInput['source_id'])

    # OLD WAY -- waiting for transform into gridfs
    # user_collection = mongoManage.getUserCollection(str(uid), str(sid))
    # getData = db[user_collection].find().sort('ts', -1)
    # getDataList = list(getData)[0]
    # del getDataList['_id']
    # return jsonify({"result": getDataList})

    # current_app.logger.info(fs.list())
    fid = uid + sid
    fileFromFID = fs.find_one({"fid": fid})
    # current_app.logger.info(fileFromFID.read())
    # use by fileFromFID.read()
    if fileFromFID != None:
        filename = fileFromFID.filename
        fileExtension = getFileExtension(filename)

        if fileExtension == "xls" or fileExtension == "xlsx" or fileExtension == "xlsm":
            try:
                # NOW IS OPENPYXL IS NOT SUPPORTED XLS EXTENSION EXCEPT xlrd (lib)

                book = xlrd.open_workbook(file_contents=fileFromFID.read())
                # current_app.logger.info("The number of worksheets is {0}".format(book.nsheets) + "\nWorksheet name(s): {0}".format(book.sheet_names()))
                content = []
                chunkData = {}
                for wsx in range(book.nsheets):
                    sh = book.sheet_by_index(wsx)
                    # current_app.logger.info("Worksheet: {0}".format(wsx+1) + "\nname: {0} \nrow: {1} \ncol: {2}".format(sh.name, sh.nrows, sh.ncols))
                    table = []
                    for rx in range(sh.nrows):
                        table.append([str(item) for item in sh.row_values(rowx=rx)])
                    chunkData[str(wsx+1) + '_' + sh.name] = makeObject(table)[0]

                return jsonify({"result": chunkData})
            except IOError as e:
                current_app.logger.info('this IO:', e)
                return jsonify({"result": "false, Error"})
                # return False
            except Exception as f:
                current_app.logger.info(f)
                return jsonify({"result": "false, Error"})
                # return False
        elif fileExtension == "csv":
            # OPEN FILE AND SAVE IN table (VARIABLE)
            try:
                table = []
                fileReader = csv.reader(StringIO(fileFromFID.read()))
                for row in fileReader:
                    table.append(row)
                # current_app.logger.info(table)
                content = makeObject(table)
                return jsonify({"result": content})
                # resultStatus = MongoInsert(str(userID), str(srcID), content)
                # return resultStatus # True or False
            except IOError as e:
                current_app.logger.info('this IO:', e)
                return jsonify({"result": "false, Error"})
            except Exception as f:
                current_app.logger.info(f)
                return jsonify({"result": "false, Error"})
                # return False
        elif fileExtension == "txt":
            pass
        elif fileExtension == "xml":
            try:
                widthSize = 0
                heightSize = 0
                header = OrderedDict()

                for event, elem in xmlet.iterparse(StringIO(fileFromFID.read())):
                    parent = elem.tag

                    for idx, val in enumerate(elem):
                        child = val.tag
                        child_text = val.text

                        if len(val) == 0: # last child node
                            try:
                                header[parent+'_'+str(idx+1)+'_'+child].append(child_text)
                            except KeyError:
                                header[parent+'_'+str(idx+1)+'_'+child] = []
                                header[parent+'_'+str(idx+1)+'_'+child].append(child_text)

                widthSize = len(header)
                for v in header.itervalues():
                    heightSize = len(v) if len(v) >= heightSize else heightSize

                # current_app.logger.info(str(widthSize) + 'x' + str(heightSize))
                headerSorted = sorted(header, key=lambda x: len(header[x]))

                newHeader = OrderedDict()
                firstRowHeader = []
                for head in headerSorted:
                    newHeader[head] = header[head]
                    firstRowHeader.append(head.split('_', 2)[2])

                # FOR FULLFILL DATA TABLE WORKS:
                for key in newHeader:
                    lastRow = len(newHeader[key])-1
                    while len(newHeader[key]) < heightSize:
                        newHeader[key].append(newHeader[key][lastRow])

                # TRANSPOSE ARRAY
                table = []
                for k, v in newHeader.items():
                    table.append(v)
                table = zip(*table)
                table.insert(0, firstRowHeader)

                content = makeObject(table)
                return jsonify({"result": content})

            except IOError as e:
                current_app.logger.info('this IO:', e)
                return jsonify({"result": "false, Error"})
            except Exception as f:
                current_app.logger.info(f)
                return jsonify({"result": "false, Error"})
                # return False
        else:
            return jsonify({"result": "false, Error"})
            # return False
    else:
        return jsonify({"result": "File not found"})

def makeObject(arrayTable):
    dataObject = []
    keyHeader = []
    valueType = []
    chunkData = {}

    # letters = list(string.ascii_uppercase)
    # letters.extend([i+b for i in letters for b in letters])

    for idx, eachArr in enumerate(arrayTable):
        # version 1
        '''
        if idx == 0:
            keyHeader = list(eachArr)
        elif idx == 1:
            valueType = list(eachArr)
        else: # idx == 2 and so on...
            chunkData = {}
            for idxInternal, value in enumerate(eachArr):
                inEachKey = {}
                inEachKey['type'] = valueType[idxInternal]
                inEachKey['value'] = value
                chunkData[keyHeader[idxInternal]] = inEachKey
            chunkData.append(chunkData)
        '''

        # version 2
        for idxInternal, value in enumerate(eachArr):
            inEachKey = []
            inEachKey.append(getDataType(value))
            inEachKey.append(value.decode("unicode_escape"))
            # bug is more than index (more ZZ value)
            # how to fix this bug -> try-except in create extent letter
            chunkData[excel_col(idxInternal)+'_'+str(idx+1)] = inEachKey
    chunkData['ts'] = datetime.strptime(datetime.now(TZ).strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")
    dataObject.append(chunkData)
    return dataObject

'''
XLS DETECT TYPE
# for rx in range(2):
#     for cx in range(2):
#         cellValue = sh.cell_value(rowx=rx, colx=cx)
# FOR DETECT TYPE INTEGRADED
#         cellType = sh.cell_type(rowx=rx, colx=cx)
#         if cellType == 0:
#             current_app.logger.info("empty")
#         elif cellType == 1:
#             current_app.logger.info("text")
#         elif cellType == 2:
#             current_app.logger.info("number")
#         elif cellType == 3:
#             current_app.logger.info("date")
#         elif cellType == 4:
#             current_app.logger.info("boolean")
#         elif cellType == 5:
#             current_app.logger.info("error")
#         elif cellType == 6:
#             current_app.logger.info("blank")
'''
