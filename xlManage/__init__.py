
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify, current_app, abort, send_from_directory
from flask_cors import CORS, cross_origin
from flaskext.mysql import MySQL

from datetime import datetime
import os
import shutil
import pytz
import codecs
import pymongo
import json
import csv
from bson.json_util import dumps

app = Flask(__name__)
app.config['ALLOWED_EXTENSIONS'] = set(['xls', 'xlsm', 'xlsx', 'csv', 'txt', 'xml'])
CORS(app)

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '@kla12131415@'
app.config['MYSQL_DATABASE_DB'] = 'intelligist'
app.config['MYSQL_DATABASE_HOST'] = '203.154.58.151'
mysql = MySQL()
mysql.init_app(app)

mongo = pymongo.MongoClient('203.154.58.151',
                            27017,
                            username='root',
                            password='@Kla12131415@',
                            authSource='admin',
                            authMechanism='SCRAM-SHA-1')
db = mongo.WorkbenchDB

TZ = pytz.timezone('Asia/Bangkok')
UPLOAD_PATH = './uploads/'

def toJson(data,columns):
    results = []
    for row in data:
        results.append(dict(zip(columns, row)))
    return results

def getFileExtension(filename):
    return filename.split('.').pop()

def getUserID (user):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id FROM user WHERE email=%s and firstname=%s and lastname=%s"
    cursor.execute(sql, (user['email'], user['firstname'], user['lastname']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0 :
        return result[0]['user_id']
    else:
        return jsonify({"status": "Failed, not found userid"})

def getDataType(val):
    try:
        float(val)
        if '.' in val: return 'float'
        else: return 'int'
    except ValueError:
        return 'string'
