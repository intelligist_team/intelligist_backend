#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import shutil
from zipfile import ZipFile
from zipfile import BadZipfile

# import xlsxwriter
import openpyxl
import xlrd

from dbConfig import *
import datetime

@app.route('/saveScriptConfig', methods=['POST'])
def saveScriptConfig():

    dataInput = request.json
    user = dataInput['user']
    script = dataInput['script_name']
    scriptType = script.split('.')[-1]
    script = ''.join(script.split('.')[0]) + '.script'

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email=%s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0:
        sql = "SELECT script_id FROM intelligent_script WHERE user_id=%s AND script_name=%s"
        cursor.execute(sql,(result[0]['user_id'], script))
        script_data = cursor.fetchall()
        scolumns = [column[0] for column in cursor.description]
        checkExistScript = toJson(script_data,scolumns)
        if len(checkExistScript) == 0:
            sql = "INSERT INTO intelligent_script (user_id, script_name) VALUES (%s, %s)"
            cursor.execute(sql,(result[0]['user_id'], script))
            conn.commit()
            cursor.close()
            return jsonify({"status": "success"})
        else:
            conn.commit()
            cursor.close()
            return jsonify({"status": "exist file name"})
    else:
        conn.commit()
        cursor.close()
        return jsonify({"status": "not found user"})

@app.route('/addSourceFromScript', methods=['POST'])
def addSourceFromScript():

    dataInput = request.json
    user = dataInput['user']
    script = user['script_name']
    scriptType = script.split('.')[-1]
    script = ''.join(script.split('.')[0]) + '.script'

    uid = str(getUserID(dataInput['user']))

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT script_id FROM intelligent_script WHERE user_id=%s AND script_name=%s"
    cursor.execute(sql,(uid, script))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    scriptIDResult = toJson(data,columns)

    sql = "SELECT id FROM intelligent_script_to_source WHERE script_id=%s AND source_name=%s"
    cursor.execute(sql,(scriptIDResult[0]['script_id'], script))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    checkExistScript = toJson(data,columns)

    if len(checkExistScript) == 0:
        if len(scriptIDResult) != 0:
            sql = "SELECT MAX(source_id) FROM intelligent_script_to_source"
            cursor.execute(sql)
            data = cursor.fetchall()
            sourceID = data[0][0]

            if sourceID == None:
                sourceID = 9990001
                sql = "INSERT INTO intelligent_script_to_source (script_id, source_id, source_name) VALUES (%s, %s, %s)"
                cursor.execute(sql,(scriptIDResult[0]['script_id'], sourceID, script))
                conn.commit()
                cursor.close()
                return (str(sourceID))
            else:
                sourceID = sourceID + 1
                sql = "INSERT INTO intelligent_script_to_source (script_id, source_id, source_name) VALUES (%s, %s, %s)"
                cursor.execute(sql,(scriptIDResult[0]['script_id'], sourceID, script))
                conn.commit()
                cursor.close()
                return (str(sourceID))
        # =====================================================
        # ===== In case of create new source from metadata ====
        # =======================vvvvv=========================
        # else:
        #     sourceID = str(dataInput['source_id'])
        #     sql = "SELECT source_name FROM metadataTable WHERE user_id=%s AND source_id=%s"
        #     cursor.execute(sql,(uid, sourceID))
        #     data = cursor.fetchall()
        #     columns = [column[0] for column in cursor.description]
        #     resultSource = toJson(data,columns)
        #
        #     sql = "INSERT INTO intelligent_script_to_source (script_id, source_id, source_name) VALUES (%s, %s, %s)"
        #     cursor.execute(sql,('', sourceID, resultSource[0]['source_name']))
    return 'done none'

@app.route('/extractBin', methods=['POST'])
def extractBin():

    callUser = request.json['user']
    userID = getUserID(callUser)

    fileMacro = callUser['macroName']
    fileType = fileMacro.split('.')[-1]
    fileMacro = fileMacro.replace(fileType, '')

    vba_filename = 'vbaProject.bin'

    #   path to uploaded macro & script
    path = 'uploads/' + str(userID) + '/' + fileMacro + 'script/'

    xlsm_file = path + callUser['macroName']

    try:
        # Open the Excel xlsm file as a zip file.
        xlsm_zip = ZipFile(xlsm_file, 'r')

        # Read the xl/vbaProject.bin file.
        vba_data = xlsm_zip.read('xl/' + vba_filename)

        # Write the vba data to a local file.
        vba_file = open(vba_filename, "wb")
        vba_file.write(vba_data)
        vba_file.close()

    except IOError:
        # Use exc_info() for Python 2.5+ compatibility.
        e = sys.exc_info()[1]
        print("File error: %s" % str(e))
        exit()

    except KeyError:
        # Usually when there isn't a xl/vbaProject.bin member in the file.
        e = sys.exc_info()[1]
        print("File error: %s" % str(e))
        print("File may not be an Excel xlsm macro file: '%s'" % xlsm_file)
        exit()

    except BadZipfile:
        # Usually if the file is an xls file and not an xlsm file.
        e = sys.exc_info()[1]
        print("File error: %s: '%s'" % (str(e), xlsm_file))
        print("File may not be an Excel xlsm macro file.")
        exit()

    except:
        # Catch any other exceptions.
        e = sys.exc_info()[1]
        print("File error: %s" % str(e))
        exit()

    shutil.move('vbaProject.bin',path+'/vbaProject.bin')

    return 'done, extract'

@app.route('/getScriptDir', methods=['POST'])
def getscriptdir():

    dataInput = request.json
    user = dataInput['user']

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email = %s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0 :
        sql = "SELECT script_id, script_name FROM intelligent_script WHERE user_id = %s"
        cursor.execute(sql, (result[0]['user_id']))
        dataScript = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        resultScript = toJson(dataScript, columns)
        conn.commit()
        cursor.close()
        return jsonify(resultScript)

@app.route('/getFileDir', methods=['POST'])
def getfiledir():

    dataInput = request.json
    user = dataInput['user']

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email = %s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0 :
        sql = "SELECT source_id, source_name FROM metadataTable WHERE user_id = %s"
        cursor.execute(sql, (result[0]['user_id']))
        dataScript = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        resultScript = toJson(dataScript, columns)
        conn.commit()
        cursor.close()
        return jsonify(resultScript)


@app.route('/embedbin', methods=['POST'])
def embedbin():

    callUser = request.json['user']
    userID = getUserID(callUser)

    path = 'uploads/' + str(userID) + '/'

    sourceWb = xlrd.open_workbook(path+'temp180001.xlsx')
    # create destination file
    destinationWb = xlsxwriter.Workbook(path+'DataTestDestination.xlsm')
    destinationWs = destinationWb.add_worksheet('Report')

    for wsx in range(sourceWb.nsheets):
        sourceSh = sourceWb.sheet_by_index(wsx)
        sourceTable = []
        for rx in range(sourceSh.nrows):
            sourceTable.append([str(item) for item in sourceSh.row_values(rowx=rx)])
        # chunkData[str(wsx+1) + '_' + sourceSh.name] = makeObject(sourceTable)[0]

    # add macro from folder xxxxxx/vbaProject.bin
    destinationWb.add_vba_project(path+'MacroTest.script/vbaProject.bin')

    row = 0
    col = 0

    for blankcell, calendar, custKey, materialGroup, totalValue in (sourceTable):
        destinationWs.write(row, col, blankcell)
        destinationWs.write(row, col+1, calendar)
        destinationWs.write(row, col+2, custKey)
        destinationWs.write(row, col+3, materialGroup)
        destinationWs.write(row, col+4, totalValue)
        row+=1

    return 'done, embed'

@app.route('/deleteScript', methods=['POST'])
def deletescript():

    dataInput = request.json
    user = dataInput['user']
    script = dataInput['intelligent_script']

    userID = getUserID(user)

    path = os.getcwd() + '/uploads/' + str(userID) + '/' + script['script_name']
    shutil.rmtree(path)

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email=%s"
    cursor.execute(sql,(user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) != 0:
        sql = "DELETE FROM intelligent_script WHERE script_name = %s"
        cursor.execute(sql,(script['script_name']))
        conn.commit()
        cursor.close()
        return jsonify({"status": "success"})
    else:
        conn.commit()
        cursor.close()
        return jsonify({"status": "user not found"})

@app.route('/getdatafromid', methods=['POST'])
def getdatafromid():

    dataInput = request.json
    user = dataInput['user']

    uid = getUserID(user)

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT script_id, script_name, latest_update FROM intelligent_script WHERE user_id=%s"
    cursor.execute(sql,uid)
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    resultScript = toJson(data,columns)

    if len(resultScript) == 0:
        return jsonify({"status": "script not found"})
    else:
        resultSource = []
        for counter in (range(len(resultScript))):
            sql = "SELECT script_id, source_id FROM intelligent_script_to_source WHERE script_id=%s"
            cursor.execute(sql,(resultScript[counter]['script_id']))
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            resultSource = resultSource + toJson(data,columns)

    resultSourceAndScript = []

    for indexDictScript in resultScript:
        for indexDictSource in resultSource:
            if indexDictScript['script_id'] == indexDictSource['script_id']:
                indexDictSource.update(indexDictScript)
                resultSourceAndScript.append(indexDictSource)

    for i in (range(len(resultSourceAndScript))):
        resultSourceAndScript[i]['source_name'] = getSourceName(resultSourceAndScript[i]['source_id'], uid)

    conn.commit()
    cursor.close()

    return jsonify(resultSourceAndScript)

def getSourceName(sid,uid):
    conn = mysql.connect()
    cursor = conn.cursor()
    sourceID = str(sid)
    if sourceID[:3] == '999':
        sql = "SELECT source_name FROM intelligent_script_to_source WHERE source_id=%s"
        cursor.execute(sql, sourceID)
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data,columns)
    else:
        sql = "SELECT source_name FROM metadataTable WHERE user_id=%s AND source_id=%s"
        cursor.execute(sql, (uid, sourceID))
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data,columns)
    conn.commit()
    cursor.close()

    return result[0]['source_name']

# def getSourceID(sname,uid):
#     conn = mysql.connect()
#     cursor = conn.cursor()
#     sourceID = str(sid)
#     sql = "SELECT metadataTable.source_id, intelligent_script_to_source.source_id FROM metadataTable LEFT JOIN intelligent_script_to_source WHERE user_id=%s AND source_name=%s"
#     cursor.execute(sql, (uid, sourceID))
#     data = cursor.fetchall()
#     columns = [column[0] for column in cursor.description]
#     result = toJson(data,columns)
#     conn.commit()
#     cursor.close()
#
#     return result[0]['source_name']


@app.route('/addmatch', methods = ["POST"])
def addMatch():

    dataInput = request.json
    user = dataInput['user']
    sendData = dataInput['sendData']

    dataName = sendData['data']
    scriptName = sendData['script']

    if data != '' and script != '':
        conn = mysql.connect()
        cursor = conn.cursor()

        sql = "INSERT INTO intelligent_script_to_source (script_id, source_id, source_name) VALUES (%s, %s, %s)"
        cursor.execute(sql, data)
        data = cursor.fetchall()
        conn.commit()
        cursor.close()

        return jsonify({"status": "success"})
    else:
        return jsonify({"status": "data not found"})


# @app.route('/openpyxl', methods=['POST'])
@app.route('/openpyxl')
def opx():
    # userID = getUserID(request.form['email'], re  quest.form['firstname'], request.form['lastname'])
    # if(userID != "not found"):
    #     path = 'uploads/' + userID + '/'
    thisdir = os.getcwd()
    path = thisdir+'/uploads/9/'

    wbsource = openpyxl.load_workbook(path+'DataTestDestination_source.xlsx').active
    wssource = wbsource[1]
    wbdest = openpyxl.load_workbook(path+'demo.xlsm', keep_vba=True).active

    wbdestvba = xlsxwriter.Workbook(path+'demo.xlsm')

    wbdestvba.add_vba_project('./vbaProject.bin')

    # # Get all data from source to DOM in "rows"
    # rows = []
    # for rowindex, val in enumerate(wssource):
    #     row_data = []
    #     for cell, val in enumerate(rowindex):
    #         row_data.append(cell.value)
    #     rows.append(row_data)

    return 'Hi'
