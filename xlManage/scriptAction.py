#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xlrd
# import xlsxwriter
from dbConfig import *

@app.route('/getTableHeader', methods=['POST'])
def gettableheader():

    dataInput = request.json
    uid = str(getUserID(dataInput['user']))

    try:
        sid = str(dataInput['source_id'])
        scriptName = 'U' + uid + '_' + sid + '.xlsx'
    except:
        fileMacro = dataInput['user']['macroName']
        fileMacro = fileMacro.split('.')
        scriptName = fileMacro + 'script/' + dataInput['user']['macroName']

    # if len(dataInput['source_id']) == 0:
    #     scriptName = dataInput['user']['macroName'] + '.script/' + dataInput['user']['macroName']
    # else:
    #     sid = str(dataInput['source_id'])
    #     scriptName = 'U' + uid + '_' + sid + '.xlsx'

    headRow = 0
    rx = 0

    path = 'uploads/' + uid + '/'

    book = xlrd.open_workbook(path + scriptName)
    content = []
    chunkData = {}
    for wsx in range(book.nsheets):
        sh = book.sheet_by_index(wsx)
        for rx in range(sh.nrows):
            if getColsFromRowNoNull(sh, rx) > getColsFromRowNoNull(sh, rx - 1):
                headRow = rx

        # print(sh.row_values(rowx = headRow))
    return jsonify(sh.row_values(rowx = headRow))

        # elif fileExtension == "csv":
        #     pass
        # elif fileExtension == "txt":
        #     pass
        # elif fileExtension == "xml":
        #     pass
        # else:
        #     return jsonify({"result": "false, Error"})
        #     # return False
    # else:
    #     return jsonify({"result": "File not found"})

def getColsFromRowNoNull(sh, rowx):
    colx = len(sh.row_values(rowx = rowx)) - (sh.row_values(rowx = rowx)).count('')
    return colx
