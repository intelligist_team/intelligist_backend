
from dbConfig import *
from dbConfig.register import *
from dbConfig.login import *
from dbConfig.fileUpload import *
from dbConfig.mongoManage import *
from dbConfig.mssqlManage import *
from dbConfig.mysqlManage import *
from managementConsole import *
from managementConsole.dashboardMgmt import *
from managementConsole.groupsMgmt import *
from managementConsole.rolesMgmt import *
from managementConsole.usersMgmt import *
from metadataTab import *
from metadataTab.metadata import *
from metadataTab.workbench import *
from xlManage import *
from xlManage.vbaManage import *
from xlManage.scriptAction import *
from scheduleJobs import *
from scheduleJobs.testScheduleMgmt import *

@app.route('/h', methods=['GET'])
def hello():
    current_app.logger.info("test Hello")
    return 'hello2'

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', threaded = True)
