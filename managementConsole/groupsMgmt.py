#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *

def updateGroupUID(group, user_id):

    # IDEA: make a set; db as A, req. as B
    # หาว่าอะไรไม่มีใน req. = ลบ
    # ex: A - B is A.difference(B)
    # หาว่าอะไรไม่มี ใน db = add
    # ex: B - A is B.difference(A)
    # หาว่าอะไรมี = update/pass
    # ex: A intersect B is A.intersection(B) this for update/pass

    try:
        group_id = str(group['group_id'])

        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "SELECT uid FROM user_list_group WHERE group_id = %s"
        cursor.execute(sql, (group_id))
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        dbResult = toJson(data, columns)
        conn.commit()
        cursor.close()

        arrayDBResult = [str(each['uid']) for each in dbResult]
        setDBResult = set(arrayDBResult)
        arrayRequestGroups = group['users_id']
        setRequestGroup = set(arrayRequestGroups)

        justDBset = setDBResult - setRequestGroup # DELETE
        justRequestSet = setRequestGroup - setDBResult # INSERT

        if len(justDBset) != 0:
            conn = mysql.connect()
            cursor = conn.cursor()
            for uid in justDBset:
                sql = "DELETE FROM user_list_group WHERE group_id = %s AND uid = %s"
                cursor.execute(sql, (group_id, uid))
            conn.commit()
            cursor.close()
        if len(justRequestSet) != 0:
            conn = mysql.connect()
            cursor = conn.cursor()
            for uid in justRequestSet:
                sql = "INSERT INTO user_list_group VALUES (NULL, %s, %s)"
                cursor.execute(sql, (group_id, uid))
            conn.commit()
            cursor.close()
        if 'group_name' in group:
            group_name = str(group['group_name'])
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "UPDATE list_group SET group_name = %s WHERE group_id = %s AND owner_uid = %s"
            cursor.execute(sql, (group_name, group_id, user_id))
            conn.commit()
            cursor.close()

        return 'success'
    except Exception as e:
        return 'Error: ' + str(e)


def deleteAGroup(user_id, group_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    list_group_affected_rows = 0
    pre_add_group_affected_rows = 0
    user_list_group_affected_rows = 0
    sql = "DELETE FROM list_group WHERE owner_uid = %s AND group_id = %s"
    list_group_affected_rows = cursor.execute(sql, (str(user_id), str(group_id)))
    if list_group_affected_rows != 0:
        sql = "DELETE FROM pre_add_group WHERE group_id = %s"
        pre_add_group_affected_rows = cursor.execute(sql, (str(group_id)))
        sql = "DELETE FROM user_list_group WHERE group_id = %s"
        user_list_group_affected_rows = cursor.execute(sql, (str(group_id)))
    conn.commit()
    cursor.close()
    return list_group_affected_rows, pre_add_group_affected_rows, user_list_group_affected_rows

@app.route('/mgmt/addGroup', methods=['POST'])
def addGroup():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        groupInfo = dataInput['group'] # group_name

        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "SELECT * FROM list_group WHERE owner_uid = %s"
        cursor.execute(sql, user_id)
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data, columns)
        conn.commit()
        cursor.close()
        dupFlag = False
        if len(result) != 0:
            for row in result:
                if row['group_name'] == groupInfo['group_name']:
                    dupFlag = True
        if dupFlag:
            return jsonify({"result": "Error: Your group is duplicate"})

        conn = mysql.connect()
        cursor = conn.cursor()
        # group_id, group_name, owner_uid, last_updated
        sql = "INSERT INTO list_group VALUES (NULL, %s, %s, NULL)"
        affected_rows = cursor.execute(sql, (groupInfo['group_name'], user_id))
        conn.commit()
        cursor.close()
        if affected_rows != 0:
            return jsonify({"result": "success"})
        else:
            return jsonify({"result": "Not have affected"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/deleteGroup', methods=['POST'])
def deleteGroup():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)
        group = dataInput['group']
        group_id = str(group['group_id'])

        '''
        # IDEA:
            if owner is root = can delete admin, user
            if owner is admin = can delete user only owner
        '''

        if lvl == '1' or lvl == '2': # Root or Admin
            list_group_affected_rows, pre_add_group_affected_rows, user_list_group_affected_rows = deleteAGroup(user_id, group_id)
            if list_group_affected_rows != 0 or pre_add_group_affected_rows != 0 or user_list_group_affected_rows != 0 :
                return jsonify({"result": "success"})
            else:
                return jsonify({"result": "Not found your group"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/deleteGroupMore', methods=['POST'])
def deleteGroupMore():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        groups_id = dataInput['targetGroupID'] # expect: ['1', '2', '3', '4']

        '''
        # IDEA:
            if owner is root = can delete admin, user
            if owner is admin = can delete user only owner
        '''

        if lvl == '1' or lvl == '2': # Root or Admin
            for group_id in groups_id:
                list_group_affected_rows, pre_add_group_affected_rows, user_list_group_affected_rows = deleteAGroup(user_id, group_id)
            return jsonify({"result": "success"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/editGroup', methods=['POST'])
def editGroup():
    # IDEA: edit name
    try:
        dataInput = request.json
        user = dataInput['user'] # email, firstname, lastname
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        groupInfo = dataInput['group'] # group_id, group_name

        if lvl == '1' or lvl == '2': # Root or Admin
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT * FROM list_group WHERE owner_uid = %s AND group_id = %s"
            cursor.execute(sql, (str(user_id), str(groupInfo['group_id'])))
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            if len(result) != 0:
                sql = "UPDATE list_group SET group_name = %s WHERE owner_uid = %s AND group_id = %s"
                affected_rows = cursor.execute(sql, (str(groupInfo['group_name']), str(user_id), str(groupInfo['group_id'])))
                conn.commit()
                cursor.close()
                return jsonify({"result": "success"})
            else:
                conn.commit()
                cursor.close()
                return jsonify({"result": "Error: Not found your request group"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/editGroupMember', methods=['POST'])
def editGroupMember():
    try:
        dataInput = request.json
        user = dataInput['user'] # email, firstname, lastname
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)
        if lvl == '1' or lvl == '2': # Root or Admin
            groupInfo = dataInput['group'] # group_id, group_name, users_id
            return jsonify({"result": updateGroupUID(groupInfo, user_id)})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# working
@app.route('/mgmt/getGroup', methods=['POST'])
def getGroup():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        '''
        # IDEA:
            if owner is root = can select admin, user
            if owner is admin = can select user only owner
        '''

        # if lvl == '1': # Root
        #     conn = mysql.connect()
        #     cursor = conn.cursor()
        #     sql = "SELECT * FROM list_group"
        #     cursor.execute(sql)
        #     data = cursor.fetchall()
        #     columns = [column[0] for column in cursor.description]
        #     result = toJson(data, columns)
        #     conn.commit()
        #     cursor.close()
        #     if len(result) != 0 :
        #         return jsonify({"result": result})
        #     else:
        #         return jsonify({"result": "Not found user infomation"})

        # SELECT list_group.group_id, user_list_group.uid, list_group.group_name, list_group.menu_available, list_group.last_updated FROM user_list_group JOIN list_group ON user_list_group.group_id = list_group.group_id WHERE owner_uid = 16

        if lvl == '1' or lvl == '2': # Root or Admin
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT * FROM list_group WHERE owner_uid = %s"
            cursor.execute(sql, user_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)

            if len(result) != 0 :
                i = 0
                for row in result:
                    sql = "SELECT user.user_id, user.firstname, user.lastname FROM user WHERE user.user_id IN (SELECT uid FROM user_list_group WHERE group_id = %s)"
                    cursor.execute(sql, row['group_id'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    userInGroup = toJson(data, columns)
                    result[i]['userInGroup'] = userInGroup
                    i = i + 1
                conn.commit()
                cursor.close()
                return jsonify({"result": result})
            else:
                conn.commit()
                cursor.close()
                return jsonify({"result": "Not found your group"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# we not have menu_available in group
# THIS FOR USER TO OWN ROLE
@app.route('/mgmt/getOwnGroups', methods=['POST'])
def getOwnGroups():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)
        if lvl == '3': # user only
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT list_group.menu_available FROM list_group WHERE group_id IN (SELECT group_id FROM user_list_group WHERE uid = %s)"
            cursor.execute(sql, user_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            conn.commit()
            cursor.close()
            if len(result) != 0:
                menu = []
                for row in result:
                    menu += row['menu_available'].split(',')

                return jsonify({"result": list(set(menu))}) # Expect return: [] or ['1', '2', '3', '4']
            else:
                return jsonify({"result": []}) # Expect return: []
        else:
            return jsonify({"result": "Just for user level"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})
