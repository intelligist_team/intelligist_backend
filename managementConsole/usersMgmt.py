#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *
from dbConfig import register

def updateDeleteUser(user_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "DELETE FROM pre_add_owner_user WHERE email IN (SELECT email FROM user WHERE user_id = %s)"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM user WHERE user_id = %s"
    affected_rows = cursor.execute(sql, user_id)
    sql = "DELETE FROM categories_permission WHERE user_id = %s"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM dashboard_permission WHERE user_id = %s"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM owner_user WHERE user_uid = %s"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM user_list_group WHERE uid = %s"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM user_list_role WHERE uid = %s"
    cursor.execute(sql, user_id)
    conn.commit()
    cursor.close()
    return affected_rows

def updateSuspendUser(user_id):
    conn = mysql.connect()
    cursor = conn.cursor()

    sql = "SELECT status FROM user WHERE user_id = %s"
    cursor.execute(sql, (user_id))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    dbResult = toJson(data, columns)

    if len(dbResult) != 0:
        if str(dbResult[0]['status']) != '4' and str(dbResult[0]['status']) != '5': # is Pending or New status
            sql = "UPDATE user SET status = 3 WHERE user_id = %s" # suspended status is 3
            affected_rows = cursor.execute(sql, user_id)
            conn.commit()
        else:
            affected_rows = updateDeleteUser(user_id)
    cursor.close()
    return affected_rows

def updateNotSuspendUser(user_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "UPDATE user SET status = 1 WHERE user_id = %s" # offline status is 1
    affected_rows = cursor.execute(sql, user_id)
    conn.commit()
    cursor.close()
    return affected_rows

def updateInfo(user, user_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "UPDATE user SET "
    sql += "email = '" + str(user['email'])
    sql += "', firstname = '" + str(user['firstname'])
    sql += "', lastname = '" + str(user['lastname'])
    sql += "', privilege_level = '" + str(user['privilege_level'])
    sql += "', job_title = '" + str(user['job_title'])
    sql += "' WHERE user_id = " + str(user_id)
    affected_rows = cursor.execute(sql)
    conn.commit()
    cursor.close()
    return affected_rows

def updateRoleID(roles_id, user_id):

    # IDEA: make a set; db as A, req. as B
    # หาว่าอะไรไม่มีใน req. = ลบ
    # ex: A - B is A.difference(B)
    # หาว่าอะไรไม่มี ใน db = add
    # ex: B - A is B.difference(A)
    # หาว่าอะไรมี = update/pass
    # ex: A intersect B is A.intersection(B) this for update/pass

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT role_id FROM user_list_role WHERE uid = %s"
    cursor.execute(sql, (user_id))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    dbResult = toJson(data, columns)
    conn.commit()
    cursor.close()

    arrayDBResult = [str(role.values()[0]) for role in dbResult]
    setDBResult = set(arrayDBResult)
    arrayRequestRoles = [str(role_id) for role_id in roles_id]
    # arrayRequestRoles = [str(role['role_id']) for role in roles]
    setRequestRole = set(arrayRequestRoles)
    justDBset = setDBResult - setRequestRole # DELETE
    justRequestSet = setRequestRole - setDBResult # INSERT

    if len(justDBset) != 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        for val in justDBset:
            sql = "DELETE FROM user_list_role WHERE role_id = %s AND uid = %s"
            cursor.execute(sql, (val, user_id))
        conn.commit()
        cursor.close()
    if len(justRequestSet) != 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        for val in justRequestSet:
            sql = "INSERT INTO user_list_role VALUES (NULL, %s, %s)"
            cursor.execute(sql, (val, user_id))
        conn.commit()
        cursor.close()

    return 'success'

def updateGroup(groups_id, user_id):

    # IDEA: make a set; db as A, req. as B
    # หาว่าอะไรไม่มีใน req. = ลบ
    # ex: A - B is A.difference(B)
    # หาว่าอะไรไม่มี ใน db = add
    # ex: B - A is B.difference(A)
    # หาว่าอะไรมี = update/pass
    # ex: A intersect B is A.intersection(B) this for update/pass

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT group_id FROM user_list_group WHERE uid = %s"
    cursor.execute(sql, (user_id))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    dbResult = toJson(data, columns)
    conn.commit()
    cursor.close()

    arrayDBResult = [str(group.values()[0]) for group in dbResult]
    setDBResult = set(arrayDBResult)
    arrayRequestGroups = [str(group_id) for group_id in groups_id]
    # arrayRequestGroups = [str(group['group_id']) for group in groups]
    setRequestGroup = set(arrayRequestGroups)
    justDBset = setDBResult - setRequestGroup # DELETE
    justRequestSet = setRequestGroup - setDBResult # INSERT

    if len(justDBset) != 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        for val in justDBset:
            sql = "DELETE FROM user_list_group WHERE group_id = %s AND uid = %s"
            cursor.execute(sql, (val, user_id))
        conn.commit()
        cursor.close()
    if len(justRequestSet) != 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        for val in justRequestSet:
            sql = "INSERT INTO user_list_group VALUES (NULL, %s, %s)"
            cursor.execute(sql, (val, user_id))
        conn.commit()
        cursor.close()

    return 'success'

def clearAdminData(user_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    '''
        # Category & Dashboard Zone
    '''
    sql = "DELETE FROM categories_permission WHERE categories_id IN (SELECT categories_id FROM categories_list WHERE owner = %s)"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM dashboard_permission WHERE dashboard_id IN (SELECT dashboard_id FROM dashboard_list WHERE categories_id IN (SELECT categories_id FROM categories_list WHERE owner = %s))"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM dashboard_list WHERE categories_id IN (SELECT categories_id FROM categories_list WHERE owner = %s)"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM categories_list WHERE owner = %s"
    cursor.execute(sql, user_id)

    '''
        # Group & Role Zone
    '''
    sql = "DELETE FROM user_list_group WHERE group_id IN (SELECT group_id FROM list_group WHERE owner_uid = %s)"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM pre_add_group WHERE group_id IN (SELECT group_id FROM list_group WHERE owner_uid = %s)"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM list_group WHERE owner_uid = %s"
    cursor.execute(sql, user_id)

    sql = "DELETE FROM user_list_role WHERE role_id IN (SELECT role_id FROM list_role WHERE owner_uid = %s)"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM pre_add_role WHERE role_id IN (SELECT role_id FROM list_role WHERE owner_uid = %s)"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM list_role WHERE owner_uid = %s"
    cursor.execute(sql, user_id)

    '''
        # Owner Zone
    '''
    sql = "DELETE FROM pre_add_owner_user WHERE owner_uid = %s"
    cursor.execute(sql, user_id)
    # update other user in this admin to NEW status
    sql = "UPDATE user SET status = 5 WHERE user_id IN (SELECT user_uid FROM owner_user WHERE owner_uid = %s)"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM owner_user WHERE owner_uid = %s"
    cursor.execute(sql, user_id)

    conn.commit()
    cursor.close()
    return 'success'

def clearUserData(user_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    '''
        # Cate & Dash Zone
    '''
    sql = "DELETE FROM categories_permission WHERE user_id = %s"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM dashboard_permission WHERE user_id = %s"
    cursor.execute(sql, user_id)

    '''
        # Group & Role Zone
    '''
    sql = "DELETE FROM user_list_group WHERE uid = %s"
    cursor.execute(sql, user_id)
    sql = "DELETE FROM user_list_role WHERE uid = %s"
    cursor.execute(sql, user_id)

    '''
        # Owner Zone
    '''
    sql = "DELETE FROM owner_user WHERE user_uid = %s"
    cursor.execute(sql, user_id)

    conn.commit()
    cursor.close()
    return 'success'

def updateOwner(owner_id, user_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT id FROM owner_user WHERE owner_uid = %s AND user_uid = %s"
    cursor.execute(sql, (owner_id, user_id))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    dbResult = toJson(data, columns)

    if len(dbResult) == 0:
        sql = "INSERT INTO owner_user VALUES (NULL, %s, %s)"
        cursor.execute(sql, (owner_id, user_id))

    conn.commit()
    cursor.close()
    return 'success'

def updateStatusOffline(user_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "UPDATE user SET status = '1' WHERE user_id = %s"
    affected_rows = cursor.execute(sql, (user_id))
    conn.commit()
    cursor.close()
    return 'success'

@app.route('/mgmt/getUID', methods=['POST'])
def getUID():
    dataInput = request.json
    user = dataInput['user']
    return jsonify({"result": str(getUserID(user))})

@app.route('/mgmt/getUserLevel', methods=['POST'])
def getUserLevel():
    dataInput = request.json
    email = dataInput['user']['email']
    return jsonify({"result": str(getLevelUser(email))})

# WORK
@app.route('/mgmt/suspendUser', methods=['POST'])
def suspendUser():
    # not real delete -> suspended
    try:
        dataInput = request.json
        user = dataInput['user']
        owner = dataInput['owner']
        user_id = str(getUserIDjustEmail(user['email']))
        owner_id = str(getUserID(owner))
        lvl = getLevelUser(owner['email'])

        '''
        # IDEA:
            if owner is root = can delete admin, user
            if owner is admin = can delete user only owner
        '''

        if lvl == '1': # Root
            affected_rows = updateSuspendUser(user_id)
            if affected_rows != 0 :
                return jsonify({"result": "success"})
            else:
                return jsonify({"result": "Not found user or Not Changed"})
        elif lvl == '2': # Admin
            lenResult = checkOwner(owner_id, user_id)
            lenResultPreOwner = checkPreOwner(owner_id, user['email'])
            if lenResult != 0 or lenResultPreOwner != 0:
                # affected_rows = updateSuspendUser(user_id)
                affected_rows = updateDeleteUser(user_id)
                if affected_rows != 0 :
                    return jsonify({"result": "success"})
                else:
                    return jsonify({"result": "Not found user or Not Changed"})
            else:
                return jsonify({"result": "User can't access to edit"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/notSuspendUser', methods=['POST'])
def notSuspendUser():
    # not real delete -> suspended
    try:
        dataInput = request.json
        user = dataInput['user']
        owner = dataInput['owner']
        user_id = str(getUserID(user))
        owner_id = str(getUserID(owner))
        lvl = getLevelUser(owner['email'])

        '''
        # IDEA:
            if owner is root = can delete admin, user
            if owner is admin = can delete user only owner
        '''

        if lvl == '1': # Root
            affected_rows = updateNotSuspendUser(user_id)
            if affected_rows != 0 :
                return jsonify({"result": "success"})
            else:
                return jsonify({"result": "Not found user or Not Changed"})
        elif lvl == '2': # Admin
            lenResult = checkOwner(owner_id, user_id)
            # lenResultPreOwner = checkPreOwner(owner_id, user['email'])
            if lenResult != 0:
                affected_rows = updateNotSuspendUser(user_id)
                if affected_rows != 0 :
                    return jsonify({"result": "success"})
                else:
                    return jsonify({"result": "Not found user or Not Changed"})
            else:
                return jsonify({"result": "User can't access to edit"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# need to dev more
@app.route('/mgmt/suspendUserMore', methods=['POST'])
def suspendUserMore():
    # not real delete -> suspended
    try:
        dataInput = request.json
        users = dataInput['users']
        owner = dataInput['owner']

        users_obj = [{'uid': getUserID(eachUser), 'email': eachUser['email']} for eachUser in users]
        owner_id = str(getUserID(owner))
        lvl = getLevelUser(owner['email'])

        '''
        # IDEA:
            if owner is root = can delete admin, user
            if owner is admin = can delete user only owner
        '''

        if lvl == '1': # Root
            for user in users_obj:
                affected_rows = updateSuspendUser(user['uid'])
            return jsonify({"result": "success"})
        elif lvl == '2': # Admin
            for user in users_obj:
                lenResult = checkOwner(owner_id, user['uid'])
                lenResultPreOwner = checkPreOwner(owner_id, user['email'])
                if lenResult != 0 or lenResultPreOwner != 0:
                # if lenResult != 0 :
                    # affected_rows = updateSuspendUser(uid)
                    affected_rows = updateDeleteUser(user['uid'])
            return jsonify({"result": "success", "description": "Please check again in your owner users"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# WORK
@app.route('/mgmt/editUser', methods=['POST'])
def editUser(): # edit information
    try:
        dataInput = request.json
        user = dataInput['user']
        owner = dataInput['owner']
        user_id = str(getUserIDjustEmail(user['email']))
        owner_id = str(getUserID(owner))
        lvl = getLevelUser(owner['email'])

        '''
        # IDEA:
            if owner is root = can edit admin, user
            if owner is admin = can edit user only owner
        '''

        if lvl == '1': # Root
            oldUserLvl = str(getLevelUser(user['email']))
            newUserLvl = str(user['privilege_level'])
            if oldUserLvl != '1' and newUserLvl != '1':

                affected_rows = updateInfo(user, user_id)
                roleUpdated = updateRoleID(user['roles'], user_id)
                groupUpdated = updateGroup(user['groups'], user_id)

                if oldUserLvl == '2' and newUserLvl == '3': # admin -> user
                    newOwnerUID = str(user['owner_id'])
                    clearAdminData(user_id)
                    updateOwner(newOwnerUID, user_id)
                elif oldUserLvl == '3' and newUserLvl == '2': # user -> admin
                    clearUserData(user_id)
                    updateStatusOffline(user_id)
                elif oldUserLvl == '3' and newUserLvl == '3': # user -> user
                    oldOwnerUID = getOwnerUIDbyUserEmail(user['email'])
                    newOwnerUID = str(user['owner_id'])
                    if oldOwnerUID != newOwnerUID:
                       clearUserData(user_id)
                       updateOwner(newOwnerUID, user_id)
                       updateStatusOffline(user_id)

                if affected_rows != 0 or roleUpdated == 'success' or groupUpdated == 'success':
                    return jsonify({"result": "success"})
                else:
                    return jsonify({"result": "Not found user or Not Changed"})
            else:
                return jsonify({"result": "Cannot change anything in 'Root Admin' Privilege"})
        elif lvl == '2': # Admin
            lenResult = checkOwner(owner_id, user_id)
            if lenResult != 0 :
                affected_rows = 0 # debug
                user['privilege_level'] = '3' # set for anti-hack
                affected_rows = updateInfo(user, user_id)
                roleUpdated = updateRoleID(user['roles'], user_id)
                groupUpdated = updateGroup(user['groups'], user_id)
                if affected_rows != 0 or roleUpdated == 'success' or groupUpdated == 'success':
                    return jsonify({"result": "success"})
                else:
                    return jsonify({"result": "Not found user or Not Changed"})
            else:
                return jsonify({"result": "User can't access to edit"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# WORK
@app.route('/mgmt/getUser', methods=['POST'])
def getUser():
    try:
        dataInput = request.json
        user = dataInput['user']
        ownerEmail = user['email']
        lvl = getLevelUser(ownerEmail)
        owner_id = getUserID(user)

        '''
        # IDEA:
            if owner is root = can select admin, user
            if owner is admin = can select user only owner
        '''

        if lvl == '1': # Root
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT user_id FROM user"
            cursor.execute(sql)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            ownerResult = toJson(data, columns)
            conn.commit()
            cursor.close()

            if len(ownerResult) != 0 :
                userResult = []
                for row in ownerResult:
                    conn = mysql.connect()
                    cursor = conn.cursor()
                    sql = "SELECT user_id, email, firstname, lastname, privilege_level, job_title, status, last_updated FROM user WHERE user_id = %s"
                    cursor.execute(sql, row['user_id'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    detailResult = toJson(data, columns)

                    sql = "SELECT user_id, CONCAT(firstname, ' ', lastname) AS name FROM user WHERE user_id IN (SELECT owner_uid FROM owner_user WHERE user_uid = %s)"
                    cursor.execute(sql, row['user_id'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    ownerUserResult = toJson(data, columns)
                    if len(ownerUserResult) == 0:
                        ownerUserResult = {"name": "", "user_id": ""} # dummy user
                    else:
                        ownerUserResult = ownerUserResult[0]
                    detailResult[0]['owner'] = ownerUserResult

                    sql = "SELECT list_role.role_id, list_role.role_name, list_role.menu_available, list_role.last_updated, list_role.owner_uid FROM list_role JOIN user_list_role ON list_role.role_id = user_list_role.role_id WHERE uid = %s"
                    cursor.execute(sql, row['user_id'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    roleResult = toJson(data, columns)
                    detailResult[0]['roles'] = roleResult

                    sql = "SELECT list_group.group_id, list_group.group_name, list_group.last_updated, list_group.owner_uid  FROM list_group JOIN user_list_group ON list_group.group_id = user_list_group.group_id WHERE uid = %s"
                    cursor.execute(sql, row['user_id'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    groupResult = toJson(data, columns)
                    detailResult[0]['groups'] = groupResult

                    userResult.append(detailResult[0])

                    conn.commit()
                    cursor.close()
                return jsonify({"result": userResult, "userLevel": lvl})
            else:
                return jsonify({"result": "Not found user infomation"})
        elif lvl == '2': # Admin
            # user_list_role, user_list_group
            # SELECT user_id, email, firstname, lastname, job_title, status, role_id, last_updated FROM (SELECT user_id, email, firstname, lastname, job_title, status, last_updated FROM user WHERE user.user_id = (SELECT user_uid FROM owner_user JOIN user ON user.user_id = owner_user.owner_uid WHERE email = 'chinnawat.ch@inet.co.th')) AS user_detail JOIN user_list_role AS user_role ON user_detail.user_id = user_role.uid
            # sql = "SELECT user_id, email, firstname, lastname, job_title, status, last_updated FROM user WHERE user.user_id = (SELECT user_uid FROM owner_user JOIN user ON user.user_id = owner_user.owner_uid WHERE email = %s)"
            conn = mysql.connect()
            cursor = conn.cursor()

            sql = "SELECT user_uid FROM owner_user JOIN user ON user.user_id = owner_user.owner_uid WHERE email = %s"
            cursor.execute(sql, ownerEmail)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            ownerResult = toJson(data, columns)

            # Pre-add-owner user
            sql = "SELECT user.user_id, user.email, user.firstname, user.lastname, user.job_title, user.status, user.last_updated FROM pre_add_owner_user JOIN user ON user.email = pre_add_owner_user.email WHERE pre_add_owner_user.owner_uid = %s"
            cursor.execute(sql, owner_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            preOwnerResult = toJson(data, columns)

            conn.commit()
            cursor.close()

            userResult = []
            if len(ownerResult) != 0 :
                conn = mysql.connect()
                cursor = conn.cursor()
                for row in ownerResult:
                    sql = "SELECT user_id, email, firstname, lastname, privilege_level, job_title, status, last_updated FROM user WHERE user_id = %s"
                    cursor.execute(sql, row['user_uid'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    detailResult = toJson(data, columns)

                    sql = "SELECT list_role.role_id, list_role.role_name, list_role.menu_available, list_role.last_updated, list_role.owner_uid  FROM list_role JOIN user_list_role ON list_role.role_id = user_list_role.role_id WHERE uid = %s"
                    cursor.execute(sql, row['user_uid'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    roleResult = toJson(data, columns)
                    detailResult[0]['roles'] = roleResult

                    sql = "SELECT list_group.group_id, list_group.group_name, list_group.last_updated, list_group.owner_uid FROM list_group JOIN user_list_group ON list_group.group_id = user_list_group.group_id WHERE uid = %s"
                    cursor.execute(sql, row['user_uid'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    groupResult = toJson(data, columns)
                    detailResult[0]['groups'] = groupResult

                    userResult.append(detailResult[0])

                    conn.commit()
                cursor.close()
            if len(preOwnerResult) != 0:
                justUIDPreOwn = [int(user['user_id']) for user in preOwnerResult]
                justUIDOwn = [int(user['user_uid']) for user in ownerResult]
                cleanArray = list(set(justUIDPreOwn) - set(justUIDOwn))

                for UID in cleanArray:
                    for user in preOwnerResult:
                        if str(user['user_id']) == str(UID):
                            userResult.append(user)
            return jsonify({"result": userResult, "userLevel": lvl})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# WORK
@app.route('/mgmt/getUserRole', methods=['POST'])
def getUserRole():
    try:
        dataInput = request.json
        user = dataInput['user']
        ownerEmail = user['email']
        lvl = getLevelUser(ownerEmail)
        user_id = getUserID(user)

        '''
        # IDEA:
            if owner is root = can select only owner
            if owner is admin = can select only owner
        '''

        if lvl == '1' or lvl == '2': # Root or Admin
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT * FROM list_role WHERE owner_uid = %s"
            cursor.execute(sql, (user_id))
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            conn.commit()
            cursor.close()
            if len(result) != 0 :
                return jsonify({"result": result})
            else:
                return jsonify({"result": "Not found user infomation"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# WORK
@app.route('/mgmt/getUserGroup', methods=['POST'])
def getUserGroup():
    try:
        dataInput = request.json
        user = dataInput['user']
        ownerEmail = user['email']
        lvl = getLevelUser(ownerEmail)
        user_id = getUserID(user)

        '''
        # IDEA:
            if owner is root = can select only owner
            if owner is admin = can select only owner
        '''

        if lvl == '1' or lvl == '2': # Root or Admin
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT * FROM list_group WHERE owner_uid = %s"
            cursor.execute(sql, (user_id))
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            conn.commit()
            cursor.close()
            if len(result) != 0 :
                return jsonify({"result": result})
            else:
                return jsonify({"result": "Not found user infomation"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})
