#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *

@app.route('/mgmt/checkAdmin', methods=['POST'])
def checkAdmin():
    try:
        dataInput = request.json
        user = dataInput['user']
        return jsonify({"result": isAdmin(user)})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})
