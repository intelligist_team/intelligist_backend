#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *

# INSERT INTO `dashboard_uac`(`id`, `uid`, `dashboard_available`, `sub_dashboard_available`, `last_updated`) SELECT NULL, user_id, '*', '*', NULL FROM user

# GET OWN
@app.route('/getOwnDashboardUAC', methods=['POST'])
def getOwnDashboardUAC():
    try:
        dataInput = request.json
        user = dataInput['user']
        # email = user['email']
        # lvl = getLevelUser(email)
        user_id = getUserID(user)

        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "SELECT dashboard_available, sub_dashboard_available FROM dashboard_uac WHERE uid = %s"
        cursor.execute(sql, str(user_id))
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data, columns)
        conn.commit()
        cursor.close()

        if len(result) != 0 :
            return jsonify({"result": result[0]})
        else:
            return jsonify({"result": "Error: Not found your information"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# GET USER DASHBOARD FOR MGMT
@app.route('/mgmt/getDashboard', methods=['POST'])
def getDashboard():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        if lvl == '1' or lvl == '2':
            conn = mysql.connect()
            cursor = conn.cursor()

            # sql = "SELECT dashboard_sub_name.id AS sub_dashboard_id, sub_dashboard_name, dashboard_id, dashboard_name.dashboard_name, dashboard_sub_name.last_updated FROM dashboard_sub_name JOIN dashboard_name ON dashboard_name.id = dashboard_sub_name.dashboard_id WHERE 1"
            sql = "SELECT id as dashboard_id, dashboard_name, last_updated FROM dashboard_name WHERE 1"
            cursor.execute(sql)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            dashData = toJson(data, columns)

            sql = "SELECT id as sub_dashboard_id, sub_dashboard_name, last_updated, dashboard_id FROM dashboard_sub_name WHERE 1"
            cursor.execute(sql)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            subDashData = toJson(data, columns)

            conn.commit()
            cursor.close()

            if len(dashData) != 0:
                for eachSub in subDashData:
                    for idx, eachDash in enumerate(dashData):
                        dashData[idx]['dashboard_id'] = str(eachDash['dashboard_id'])
                        try:
                            str(dashData[idx]['sub_dashboard'])
                        except Exception as e:
                            dashData[idx]['sub_dashboard'] = []
                        if str(eachDash['dashboard_id']) == str(eachSub['dashboard_id']):
                            dashData[idx]['sub_dashboard'].append({"sub_dashboard_id": eachSub['sub_dashboard_id'], "sub_dashboard_name": eachSub['sub_dashboard_name'], "last_updated": eachSub['last_updated']})

                return jsonify({"result": dashData})
            else:
                return jsonify({"result": "Not found"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# GET ALL OWNER USER
@app.route('/mgmt/getUserDashboardUAC', methods=['POST'])
def getUserDashboardUAC():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        if lvl == '1': # root get all user including itself
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT user.user_id, user.firstname, user.lastname, dashInfo.dashboard_available, dashInfo.sub_dashboard_available, dashInfo.last_updated FROM user JOIN (SELECT dashboard_uac.uid, dashboard_uac.dashboard_available, dashboard_uac.sub_dashboard_available, dashboard_uac.last_updated FROM dashboard_uac) AS dashInfo WHERE dashInfo.uid = user.user_id AND user.status != '3' AND user.status != '4'"
            cursor.execute(sql)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            conn.commit()
            cursor.close()

            if len(result) != 0 :
                return jsonify({"result": result})
            else:
                return jsonify({"result": "Not found your information"})
        elif lvl == '2': # admin get only owner user
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT user.user_id, user.firstname, user.lastname, dashInfo.dashboard_available, dashInfo.sub_dashboard_available, dashInfo.last_updated FROM user JOIN (SELECT dashboard_uac.uid, dashboard_uac.dashboard_available, dashboard_uac.sub_dashboard_available, dashboard_uac.last_updated FROM dashboard_uac WHERE dashboard_uac.uid in (SELECT user_uid FROM owner_user WHERE owner_uid = %s)) AS dashInfo WHERE dashInfo.uid = user.user_id AND user.status != '3' AND user.status != '4'"
            cursor.execute(sql, user_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            conn.commit()
            cursor.close()

            if len(result) != 0 :
                return jsonify({"result": result})
            else:
                return jsonify({"result": "Not found your information"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/updateUserDashboardUAC', methods=['POST'])
def updateUserDashboardUAC():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        try:
            target = dataInput['target']
            targetDash = str(target['dashboard_available'])
            targetSubDash = str(target['sub_dashboard_available'])
            targetUID = str(target['uid'])
        except Exception as e:
            current_app.logger.info(e)
            return jsonify({"result": "Error: Incorrect data format (Object: 'target')"})

        if lvl == '1': # root can update all of user
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "UPDATE dashboard_uac SET dashboard_available = %s, sub_dashboard_available = %s WHERE uid = %s"
            affected_rows = cursor.execute(sql, (targetDash, targetSubDash, targetUID))
            conn.commit()
            cursor.close()
            return jsonify({"result": "success"})
        elif lvl == '2': # admin update only owner user
            if checkOwner(user_id, targetUID):
                conn = mysql.connect()
                cursor = conn.cursor()
                sql = "UPDATE dashboard_uac SET dashboard_available = %s, sub_dashboard_available = %s WHERE uid = %s"
                affected_rows = cursor.execute(sql, (targetDash, targetSubDash, targetUID))
                conn.commit()
                cursor.close()
                return jsonify({"result": "success"})
            else:
                return jsonify({"result": "Error: You can't access this account"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})


'''
    <<< ---------- >>>
    <<< NEW DESIGN >>>
    <<< ---------- >>>
'''

# get own info with others
@app.route('/getOwnWithOtherUser', methods=['POST'])
def getOwnWithOtherUser():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "SELECT user_id, email, firstname, lastname, create_at, privilege_level, job_title, last_updated FROM user WHERE user_id = %s"
        cursor.execute(sql, user_id)
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data, columns)
        cursor.close()

        if lvl == '1': # Root
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT user_id, email, firstname, lastname, create_at, privilege_level, job_title, last_updated FROM user"
            cursor.execute(sql)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            cursor.close()

        elif lvl == '2': # Admin
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT user_uid FROM owner_user JOIN user ON user.user_id = owner_user.owner_uid WHERE email = %s"
            cursor.execute(sql, email)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            ownerResult = toJson(data, columns)
            cursor.close()

            if len(ownerResult) != 0:
                conn = mysql.connect()
                cursor = conn.cursor()

                for row in ownerResult:
                    sql = "SELECT user_id, email, firstname, lastname, create_at, privilege_level, job_title, last_updated FROM user WHERE user_id = %s"
                    cursor.execute(sql, row['user_uid'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    detailResult = toJson(data, columns)
                    result.append(detailResult[0])

                cursor.close()
        else:
            return jsonify({"result": result})

        return jsonify({"result": result})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# get dashboard ของแต่ละคน
@app.route('/getDashboardList', methods=['POST'])
def getDashboardList():
    # IDEA: เช็คจาก perm Table ก่อน จากนั้้นเอา dashID/cataID ไปหารายละเอียด dashboard/catagories
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        if lvl == '1': # root get all
            conn = mysql.connect()
            cursor = conn.cursor()

            # OLD: SELECT categories_list.categories_id, categories_list.categories_name, categories_list.owner FROM categories_permission JOIN categories_list ON categories_permission.categories_id = categories_list.categories_id
            sql = "SELECT categories_list.categories_id, categories_list.categories_name, categories_list.owner FROM categories_list WHERE categories_list.categories_id IN (SELECT categories_id FROM categories_permission)"
            cursor.execute(sql)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            cateResult = toJson(data, columns)

            # OLD: SELECT dashboard_list.dashboard_id, dashboard_list.dashboard_name, dashboard_list.link, dashboard_list.categories_id, dashboard_list.owner FROM dashboard_permission JOIN dashboard_list ON dashboard_permission.dashboard_id = dashboard_list.dashboard_id
            sql = "SELECT dashboard_list.dashboard_id, dashboard_list.dashboard_name, dashboard_list.link, dashboard_list.categories_id, dashboard_list.owner FROM dashboard_list WHERE dashboard_list.dashboard_id IN (SELECT dashboard_id FROM dashboard_permission)"
            cursor.execute(sql)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            dashResult = toJson(data, columns)

            for cx, eachCate in enumerate(cateResult):
                # eachCate['categories_id']
                # eachCate['categories_name']

                sql = "SELECT user.user_id, user.firstname, user.lastname FROM user WHERE user.user_id IN (SELECT categories_permission.user_id FROM categories_permission WHERE categories_id = %s)"
                cursor.execute(sql, str(eachCate['categories_id']))
                data = cursor.fetchall()
                columns = [column[0] for column in cursor.description]
                userInCate = toJson(data, columns)
                cateResult[cx][u'userInCate'] = userInCate

                try:
                    str(cateResult[cx][u'dashboard_data'])
                except Exception as e:
                    cateResult[cx][u'dashboard_data'] = []

                for dx, eachDash in enumerate(dashResult):
                    if str(eachDash['categories_id']) == str(eachCate['categories_id']):
                        tmpDash = dict(eachDash)
                        if 'categories_id' in tmpDash:
                            del tmpDash['categories_id']

                        sql = "SELECT user.user_id, user.firstname, user.lastname FROM user WHERE user.user_id IN (SELECT dashboard_permission.user_id FROM dashboard_permission WHERE dashboard_id = %s)"
                        cursor.execute(sql, str(eachDash['dashboard_id']))
                        data = cursor.fetchall()
                        columns = [column[0] for column in cursor.description]
                        userInDash = toJson(data, columns)
                        tmpDash[u'userInDash'] = userInDash

                        cateResult[cx][u'dashboard_data'].append(tmpDash)

            cursor.close()
            return jsonify({"result": cateResult, "userLevel": lvl})
        elif lvl == '2': # admin just only owner
            conn = mysql.connect()
            cursor = conn.cursor()

            # OLD: SELECT categories_list.categories_id, categories_list.categories_name, categories_list.owner FROM categories_permission JOIN categories_list ON categories_permission.categories_id = categories_list.categories_id WHERE categories_permission.user_id = %s
            sql = "SELECT categories_list.categories_id, categories_list.categories_name, categories_list.owner FROM categories_list WHERE categories_list.categories_id IN (SELECT categories_id FROM categories_permission) AND owner = %s"
            cursor.execute(sql, user_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            cateResult = toJson(data, columns)

            # OLD: SELECT dashboard_list.dashboard_id, dashboard_list.dashboard_name, dashboard_list.link, dashboard_list.categories_id, dashboard_list.owner FROM dashboard_permission JOIN dashboard_list ON dashboard_permission.dashboard_id = dashboard_list.dashboard_id WHERE dashboard_permission.user_id = %s
            sql = "SELECT dashboard_list.dashboard_id, dashboard_list.dashboard_name, dashboard_list.link, dashboard_list.categories_id, dashboard_list.owner FROM dashboard_list WHERE dashboard_list.dashboard_id IN (SELECT dashboard_id FROM dashboard_permission) AND owner = %s"
            cursor.execute(sql, user_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            dashResult = toJson(data, columns)

            for cx, eachCate in enumerate(cateResult):
                # eachCate['categories_id']
                # eachCate['categories_name']

                sql = "SELECT user.user_id, user.firstname, user.lastname FROM user WHERE user.user_id IN (SELECT categories_permission.user_id FROM categories_permission WHERE categories_id = %s)"
                cursor.execute(sql, str(eachCate['categories_id']))
                data = cursor.fetchall()
                columns = [column[0] for column in cursor.description]
                userInCate = toJson(data, columns)
                cateResult[cx][u'userInCate'] = userInCate

                try:
                    str(cateResult[cx][u'dashboard_data'])
                except Exception as e:
                    cateResult[cx][u'dashboard_data'] = []

                for dx, eachDash in enumerate(dashResult):
                    if str(eachDash['categories_id']) == str(eachCate['categories_id']):
                        tmpDash = dict(eachDash)
                        if 'categories_id' in tmpDash:
                            del tmpDash['categories_id']

                        sql = "SELECT user.user_id, user.firstname, user.lastname FROM user WHERE user.user_id IN (SELECT dashboard_permission.user_id FROM dashboard_permission WHERE dashboard_id = %s)"
                        cursor.execute(sql, str(eachDash['dashboard_id']))
                        data = cursor.fetchall()
                        columns = [column[0] for column in cursor.description]
                        userInDash = toJson(data, columns)
                        tmpDash[u'userInDash'] = userInDash

                        cateResult[cx][u'dashboard_data'].append(tmpDash)

            cursor.close()
            return jsonify({"result": cateResult, "userLevel": lvl})
        else:
            conn = mysql.connect()
            cursor = conn.cursor()

            sql = """SELECT categories_list.categories_id, categories_list.categories_name FROM categories_list WHERE categories_list.categories_id IN (SELECT categories_id FROM categories_permission WHERE user_id = %s)"""
            cursor.execute(sql, user_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            cateResult = toJson(data, columns)

            sql = """SELECT dashboard_list.dashboard_id, dashboard_list.dashboard_name, dashboard_list.link, dashboard_list.categories_id FROM dashboard_list WHERE dashboard_list.dashboard_id IN (SELECT dashboard_id FROM dashboard_permission WHERE user_id = %s)"""
            cursor.execute(sql, user_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            dashResult = toJson(data, columns)


            for cx, eachCate in enumerate(cateResult):
                try:
                    str(cateResult[cx][u'dashboard_data'])
                except Exception as e:
                    cateResult[cx][u'dashboard_data'] = []

                for dx, eachDash in enumerate(dashResult):
                    if str(eachDash['categories_id']) == str(eachCate['categories_id']):
                        tmpDash = dict(eachDash)
                        if 'categories_id' in tmpDash:
                            del tmpDash['categories_id']

                        cateResult[cx][u'dashboard_data'].append(tmpDash)

            cursor.close()
            return jsonify({"result": cateResult, "userLevel": lvl})

    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# @app.route('/getAllDashboardList', methods=['POST'])
# def getAllDashboardList():
#     try:
#         dataInput = request.json
#         user = dataInput['user']
#         email = user['email']
#         lvl = getLevelUser(email)
#         user_id = getUserID(user)
#
#         if lvl == '1': # root get all
#             conn = mysql.connect()
#             cursor = conn.cursor()
#
#             sql = "SELECT categories_list.categories_id, categories_list.categories_name, categories_list.owner FROM categories_permission JOIN categories_list ON categories_permission.categories_id = categories_list.categories_id"
#             cursor.execute(sql)
#             data = cursor.fetchall()
#             columns = [column[0] for column in cursor.description]
#             cateResult = toJson(data, columns)
#
#             sql = "SELECT dashboard_list.dashboard_id, dashboard_list.dashboard_name, dashboard_list.link, dashboard_list.categories_id, dashboard_list.owner FROM dashboard_permission JOIN dashboard_list ON dashboard_permission.dashboard_id = dashboard_list.dashboard_id"
#             cursor.execute(sql)
#             data = cursor.fetchall()
#             columns = [column[0] for column in cursor.description]
#             dashResult = toJson(data, columns)
#
#             for cx, eachCate in enumerate(cateResult):
#                 # eachCate['categories_id']
#                 # eachCate['categories_name']
#
#                 sql = "SELECT user.user_id, user.firstname, user.lastname FROM user WHERE user.user_id IN (SELECT categories_permission.user_id FROM categories_permission WHERE categories_id = %s)"
#                 cursor.execute(sql, str(eachCate['categories_id']))
#                 data = cursor.fetchall()
#                 columns = [column[0] for column in cursor.description]
#                 userInCate = toJson(data, columns)
#                 cateResult[cx][u'userInCate'] = userInCate
#
#                 try:
#                     str(cateResult[cx][u'dashboard_data'])
#                 except Exception as e:
#                     cateResult[cx][u'dashboard_data'] = []
#
#                 for dx, eachDash in enumerate(dashResult):
#                     if str(eachDash['categories_id']) == str(eachCate['categories_id']):
#                         tmpDash = dict(eachDash)
#                         if 'categories_id' in tmpDash:
#                             del tmpDash['categories_id']
#
#                         sql = "SELECT user.user_id, user.firstname, user.lastname FROM user WHERE user.user_id IN (SELECT dashboard_permission.user_id FROM dashboard_permission WHERE dashboard_id = %s)"
#                         cursor.execute(sql, str(eachDash['dashboard_id']))
#                         data = cursor.fetchall()
#                         columns = [column[0] for column in cursor.description]
#                         userInDash = toJson(data, columns)
#                         tmpDash[u'userInDash'] = userInDash
#
#                         cateResult[cx][u'dashboard_data'].append(tmpDash)
#
#             cursor.close()
#             return jsonify({"result": cateResult})
#         else:
#             return jsonify({"result": "User can't access"})
#
#     except Exception as e:
#         current_app.logger.info(e)
#         return jsonify({"result": "Error: " + str(e)})

# @app.route('/testdash', methods=['POST'])
# def testdash():
#     a = str("<script type='text/javascript' src='http://203.154.135.78/javascripts/api/viz_v1.js'></script><div class='tableauPlaceholder' style='width: 1169px; height: 854px;'><object class='tableauViz' width='1169' height='854' style='display:none;'><param name='host_url' value='http%3A%2F%2F203.154.135.78%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='iNETDashboard&#47;INETDashboard' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='showAppBanner' value='false' /><param name='filter' value='iframeSizedToWindow=true' /></object></div>")
#     return jsonify({"result": a})

# DASHBOARD
@app.route('/addDashboard', methods=['POST'])
def addDashboard():
    # IDEA: เช็คจาก perm Table ก่อน จากนั้้นเอา dashID/cataID ไปหารายละเอียด dashboard/catagories
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        try:
            dashInfo = dataInput['dashboard_info']
            dashName = str(dashInfo['dashboard_name']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
            link = str(dashInfo['link']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
            cid = str(dashInfo['categories_id'])
            # dashName = str(dashInfo['dashboard_name']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
            # link = str(dashInfo['link']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
            # cid = str(dashInfo['categories_id'])
        except Exception as e:
            current_app.logger.info(e)
            return jsonify({"result": "Error: Incorrect data (Object: 'dashboard_info')"})

        if lvl == '1' or lvl == '2': # admin just only owner
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "INSERT INTO dashboard_list VALUES (NULL, %s, %s, %s, %s, NULL)"
            affected_rows = cursor.execute(sql, (dashName, user_id, link, cid))
            conn.commit()

            if (affected_rows != 0):

                # add ไป categories_dashboard but คาดว่ายังไม่ได้ใช้

                # ข้างล่างนี้ อาจจะทำเป็น function แยกไป เผื่อว่าจะใช้ตอน add perm กับ user ปกติ
                sql = "SELECT dashboard_id FROM dashboard_list WHERE owner = %s ORDER BY dashboard_id DESC"
                cursor.execute(sql, user_id)
                data = cursor.fetchall()
                columns = [column[0] for column in cursor.description]
                result = toJson(data, columns)

                sql = "INSERT INTO dashboard_permission VALUES (NULL, %s, %s)"
                affected_rows_dash_perm = cursor.execute(sql, (result[0]['dashboard_id'], user_id))
                conn.commit()
                cursor.close()
                if (affected_rows_dash_perm != 0):
                    return jsonify({"result": "success"})
                else:
                    return jsonify({"result": "fail"})
            else:
                cursor.close()
                return jsonify({"result": "fail"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

'''
/editDashboard
{
    "user": {
    	"email": "",
        "firstname": "",
        "lastname": ""
    },
    "dashboard_info": {
    	"dashboard_id": "",
    	"dashboard_name": "",
    	"link": "",
    	"categories_id": ""
    }
}
'''
@app.route('/editDashboard', methods=['POST'])
def editDashboard():
    # IDEA: เช็คจาก perm Table ก่อน จากนั้้นเอา dashID/cataID ไปหารายละเอียด dashboard/catagories
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        try:
            dashInfo = dataInput['dashboard_info']
            dashID = str(dashInfo['dashboard_id']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
            dashName = str(dashInfo['dashboard_name']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
            link = str(dashInfo['link']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
            cid = str(dashInfo['categories_id'])
        except Exception as e:
            current_app.logger.info(e)
            return jsonify({"result": "Error: Incorrect data (Object: 'dashboard_info')"})

        if lvl == '1' or lvl == '2': # admin just only owner
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "UPDATE dashboard_list SET dashboard_name = %s, link = %s, categories_id = %s WHERE owner = %s AND dashboard_id = %s"
            affected_rows = cursor.execute(sql, (dashName, link, cid, user_id, dashID))
            conn.commit()
            cursor.close()

            return jsonify({"result": "success"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/removeDashboard', methods=['POST'])
def removeDashboard():
    # IDEA: เช็คจาก perm Table ก่อน จากนั้้นเอา dashID/cataID ไปหารายละเอียด dashboard/catagories
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        try:
            dashID = str(dataInput['dashboard_id']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
        except Exception as e:
            current_app.logger.info(e)
            return jsonify({"result": "Error: Incorrect data (Object: 'dashboard_id')"})

        if lvl == '1' or lvl == '2': # admin just only owner
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "DELETE FROM dashboard_list WHERE owner = %s AND dashboard_id = %s"
            affected_rows = cursor.execute(sql, (user_id, dashID))
            if affected_rows:
                sql = "DELETE FROM dashboard_permission WHERE dashboard_id = %s"
                affected_rows_dashboard_permission = cursor.execute(sql, (dashID))
            conn.commit()
            cursor.close()

            return jsonify({"result": "success"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# CATEGORY #
@app.route('/addCategory', methods=['POST'])
def addCategory():
    # IDEA: เช็คจาก perm Table ก่อน จากนั้้นเอา dashID/cataID ไปหารายละเอียด dashboard/catagories
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        try:
            cateName = str(dataInput['category_name']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
        except Exception as e:
            current_app.logger.info(e)
            return jsonify({"result": "Error: Incorrect data (Object: 'category_name')"})

        if lvl == '1' or lvl == '2': # admin just only owner
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "INSERT INTO categories_list VALUES (NULL, %s, %s, NULL)"
            affected_rows = cursor.execute(sql, (cateName, user_id))
            conn.commit()

            if (affected_rows != 0):

                # add ไป categories_dashboard but คาดว่ายังไม่ได้ใช้

                # ข้างล่างนี้ อาจจะทำเป็น function แยกไป เผื่อว่าจะใช้ตอน add perm กับ user ปกติ
                sql = "SELECT categories_id FROM categories_list WHERE owner = %s ORDER BY categories_id DESC"
                cursor.execute(sql, user_id)
                data = cursor.fetchall()
                columns = [column[0] for column in cursor.description]
                result = toJson(data, columns)

                sql = "INSERT INTO categories_permission VALUES (NULL, %s, %s)"
                affected_rows_cate_perm = cursor.execute(sql, (result[0]['categories_id'], user_id))
                conn.commit()
                cursor.close()
                if (affected_rows_cate_perm != 0):
                    return jsonify({"result": "success"})
                else:
                    return jsonify({"result": "fail"})
            else:
                cursor.close()
                return jsonify({"result": "fail"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/editCategory', methods=['POST'])
def editCategory():
    # IDEA: เช็คจาก perm Table ก่อน จากนั้้นเอา dashID/cataID ไปหารายละเอียด dashboard/catagories
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        try:
            cateInfo = dataInput['category_info']
            cateID = str(cateInfo['category_id']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
            cateName = str(cateInfo['category_name']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
        except Exception as e:
            current_app.logger.info(e)
            return jsonify({"result": "Error: Incorrect data (Object: 'category_info')"})

        if lvl == '1' or lvl == '2': # admin just only owner
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "UPDATE categories_list SET categories_name = %s WHERE owner = %s AND categories_id = %s"
            affected_rows = cursor.execute(sql, (cateName, user_id, cateID))
            conn.commit()
            cursor.close()

            return jsonify({"result": "success"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/removeCategory', methods=['POST'])
def removeCategory():
    # IDEA: เช็คจาก perm Table ก่อน จากนั้้นเอา dashID/cataID ไปหารายละเอียด dashboard/catagories
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        try:
            cateID = str(dataInput['category_id']).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'")
        except Exception as e:
            current_app.logger.info(e)
            return jsonify({"result": "Error: Incorrect data (Object: 'category_id')"})

        if lvl == '1' or lvl == '2': # admin just only owner
            conn = mysql.connect()
            cursor = conn.cursor()

            sql = "SELECT dashboard_id FROM dashboard_list WHERE owner = %s AND categories_id = %s"
            cursor.execute(sql, (user_id, cateID))
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)

            if (result):
                dashIDs = "(" + ", ".join(["'" + str(each['dashboard_id']) + "'" for each in result]) + ")"
                sql = "DELETE FROM dashboard_list WHERE owner = %s AND categories_id = %s"
                affected_rows_dashboard_list = cursor.execute(sql, (user_id, cateID))
                sql = "DELETE FROM dashboard_permission WHERE dashboard_id in " + dashIDs
                affected_rows_dashboard_permission = cursor.execute(sql)

            sql = "DELETE FROM categories_list WHERE owner = %s AND categories_id = %s"
            affected_rows_categories_list = cursor.execute(sql, (user_id, cateID))
            if affected_rows_categories_list:
                sql = "DELETE FROM categories_permission WHERE categories_id = %s"
                affected_rows_categories_permission = cursor.execute(sql, (cateID))

            conn.commit()
            cursor.close()
            return jsonify({"result": "success"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

'''
/updateCateDashPermission
{
    "user": {
    	"email": "",
        "firstname": "",
        "lastname": ""
    },
    "permission_info": {
    	"mode": "dashboard",
    	"mode_id": "",
    	"users_id": [""]
    }
}
'''
@app.route('/updateCateDashPermission', methods=['POST'])
def updateCateDashPermission():

    # IDEA: make a set; db as A, req. as B
    # หาว่าอะไรไม่มีใน req. = ลบ
    # ex: A - B is A.difference(B)
    # หาว่าอะไรไม่มี ใน db = add
    # ex: B - A is B.difference(A)
    # หาว่าอะไรมี = update/pass
    # ex: A intersect B is A.intersection(B) this for update/pass

    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        try:
            permission_info = dataInput['permission_info']
            mode = str(permission_info['mode'])
            modeID = permission_info['mode_id']
            usersID = permission_info['users_id']
            if not type(modeID) in [int, str, unicode]:
                return jsonify({"result": "Error: Incorrect data (Object: 'mode_id' should be Integer or String)"})
            else:
                modeID = str(modeID)
            if not type(usersID) is list:
                return jsonify({"result": "Error: Incorrect data (Object: 'users_id' should be List or Array)"})
        except Exception as e:
            current_app.logger.info(e)
            return jsonify({"result": "Error: Incorrect data (Object: 'permission_info')"})

        if lvl == '1' or lvl == '2': # admin just only owner
            if mode == 'dashboard':
                try:
                    conn = mysql.connect()
                    cursor = conn.cursor()
                    sql = "SELECT user_id FROM dashboard_permission WHERE dashboard_id = %s"
                    cursor.execute(sql, (modeID))
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    dbResult = toJson(data, columns)
                    cursor.close()

                    arrayDBResult = [str(each['user_id']) for each in dbResult]
                    setDBResult = set(arrayDBResult)
                    arrayRequestUID = [str(each) for each in usersID]
                    setRequestUID = set(arrayRequestUID)

                    justDBset = setDBResult - setRequestUID # DELETE
                    justRequestSet = setRequestUID - setDBResult # INSERT

                    adjustDashboardPermission(justDBset, justRequestSet, modeID)

                    # OLD DB DATA
                    conn = mysql.connect()
                    cursor = conn.cursor()
                    sql = "SELECT categories_permission.user_id, dashboard_list.categories_id FROM dashboard_list JOIN categories_permission ON dashboard_list.categories_id = categories_permission.categories_id WHERE dashboard_list.dashboard_id = %s"
                    cursor.execute(sql, (modeID))
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    cateUIDs = toJson(data, columns)

                    sql = "SELECT dashboard_permission.user_id FROM dashboard_permission WHERE dashboard_permission.dashboard_id IN (SELECT dashboard_list.dashboard_id FROM dashboard_list WHERE dashboard_list.categories_id = (SELECT dashboard_list.categories_id FROM dashboard_list WHERE dashboard_list.dashboard_id = %s))"
                    cursor.execute(sql, (modeID))
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    dashUIDs = toJson(data, columns)
                    cursor.close()

                    arrayDBResult = [str(each['user_id']) for each in cateUIDs]
                    setDBResult = set(arrayDBResult)
                    arrayRequestUID = [str(each['user_id']) for each in dashUIDs]
                    setRequestUID = set(arrayRequestUID)

                    justDBset = setDBResult - setRequestUID # DELETE
                    justRequestSet = setRequestUID - setDBResult # INSERT

                    modeID = str(cateUIDs[0]['categories_id'])
                    adjustCategoryPermission(justDBset, justRequestSet, modeID)

                    return jsonify({"result": "success"})
                except Exception as e:
                    return jsonify({"result": "Error: " + str(e)})
            elif mode == 'category':
                try:
                    conn = mysql.connect()
                    cursor = conn.cursor()
                    sql = "SELECT user_id FROM categories_permission WHERE categories_id = %s"
                    cursor.execute(sql, (modeID))
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    dbResult = toJson(data, columns)
                    conn.commit()
                    cursor.close()

                    arrayDBResult = [str(each['user_id']) for each in dbResult]
                    setDBResult = set(arrayDBResult)
                    arrayRequestUID = [str(each) for each in usersID]
                    setRequestUID = set(arrayRequestUID)

                    justDBset = setDBResult - setRequestUID # DELETE
                    justRequestSet = setRequestUID - setDBResult # INSERT

                    adjustCategoryPermission(justDBset, justRequestSet, modeID)

                    conn = mysql.connect()
                    cursor = conn.cursor()
                    sql = "SELECT dashboard_id FROM dashboard_list WHERE categories_id = %s AND owner = %s"
                    cursor.execute(sql, (modeID, user_id))
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    dashIDs = toJson(data, columns)
                    cursor.close()

                    for each in dashIDs:
                        modeID = str(each['dashboard_id'])
                        adjustDashboardPermission(justDBset, justRequestSet, modeID)

                    return jsonify({"result": "success"})
                except Exception as e:
                    return jsonify({"result": "Error: " + str(e)})
            else:
                return jsonify({"result": "Error: wrong mode name"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

def adjustDashboardPermission(justDBset, justRequestSet, modeID):
    if len(justDBset) != 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        for uid in justDBset:
            sql = "DELETE FROM dashboard_permission WHERE dashboard_id = %s AND user_id = %s"
            cursor.execute(sql, (modeID, uid))
        conn.commit()
        cursor.close()
    if len(justRequestSet) != 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        for uid in justRequestSet:
            sql = "INSERT INTO dashboard_permission VALUES (NULL, %s, %s)"
            cursor.execute(sql, (modeID, uid))
        conn.commit()
        cursor.close()
    return 'success'

def adjustCategoryPermission(justDBset, justRequestSet, modeID):
    if len(justDBset) != 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        for uid in justDBset:
            sql = "DELETE FROM categories_permission WHERE categories_id = %s AND user_id = %s"
            cursor.execute(sql, (modeID, uid))
        conn.commit()
        cursor.close()
    if len(justRequestSet) != 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        for uid in justRequestSet:
            sql = "INSERT INTO categories_permission VALUES (NULL, %s, %s)"
            cursor.execute(sql, (modeID, uid))
        conn.commit()
        cursor.close()
    return 'success'
