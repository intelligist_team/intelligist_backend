#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *

def updateRoleUID(role, user_id):

    # IDEA: make a set; db as A, req. as B
    # หาว่าอะไรไม่มีใน req. = ลบ
    # ex: A - B is A.difference(B)
    # หาว่าอะไรไม่มี ใน db = add
    # ex: B - A is B.difference(A)
    # หาว่าอะไรมี = update/pass
    # ex: A intersect B is A.intersection(B) this for update/pass

    try:
        role_id = str(role['role_id'])

        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "SELECT uid FROM user_list_role WHERE role_id = %s"
        cursor.execute(sql, (role_id))
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        dbResult = toJson(data, columns)
        conn.commit()
        cursor.close()

        arrayDBResult = [str(each['uid']) for each in dbResult]
        setDBResult = set(arrayDBResult)
        arrayRequestRoles = role['users_id']
        setRequestRole = set(arrayRequestRoles)

        justDBset = setDBResult - setRequestRole # DELETE
        justRequestSet = setRequestRole - setDBResult # INSERT

        if len(justDBset) != 0:
            conn = mysql.connect()
            cursor = conn.cursor()
            for uid in justDBset:
                sql = "DELETE FROM user_list_role WHERE role_id = %s AND uid = %s"
                cursor.execute(sql, (role_id, uid))
            conn.commit()
            cursor.close()
        if len(justRequestSet) != 0:
            conn = mysql.connect()
            cursor = conn.cursor()
            for uid in justRequestSet:
                sql = "INSERT INTO user_list_role VALUES (NULL, %s, %s)"
                cursor.execute(sql, (role_id, uid))
            conn.commit()
            cursor.close()
        if 'role_name' in role:
            role_name = str(role['role_name'])
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "UPDATE list_role SET role_name = %s WHERE role_id = %s AND owner_uid = %s"
            cursor.execute(sql, (role_name, role_id, user_id))
            conn.commit()
            cursor.close()

        return 'success'
    except Exception as e:
        return 'Error: ' + str(e)


def deleteARole(user_id, role_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    list_role_affected_rows = 0
    pre_add_role_affected_rows = 0
    user_list_role_affected_rows = 0
    sql = "DELETE FROM list_role WHERE owner_uid = %s AND role_id = %s"
    list_role_affected_rows = cursor.execute(sql, (str(user_id), str(role_id)))
    if list_role_affected_rows != 0:
        sql = "DELETE FROM pre_add_role WHERE role_id = %s"
        pre_add_role_affected_rows = cursor.execute(sql, (str(role_id)))
        sql = "DELETE FROM user_list_role WHERE role_id = %s"
        user_list_role_affected_rows = cursor.execute(sql, (str(role_id)))
    conn.commit()
    cursor.close()
    return list_role_affected_rows, pre_add_role_affected_rows, user_list_role_affected_rows

@app.route('/mgmt/addRole', methods=['POST'])
def addRole():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        roleInfo = dataInput['role'] # role_name, menu_available

        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "SELECT * FROM list_role WHERE owner_uid = %s"
        cursor.execute(sql, user_id)
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data, columns)
        conn.commit()
        cursor.close()
        dupFlag = False
        if len(result) != 0:
            for row in result:
                # if row['role_name'] == roleInfo['role_name'] and row['menu_available'] == roleInfo['menu_available']:
                if row['role_name'] == roleInfo['role_name']:
                    dupFlag = True
        if dupFlag:
            return jsonify({"result": "Error: Your role is duplicate"})

        conn = mysql.connect()
        cursor = conn.cursor()
        # role_id, role_name, menu_available, owner_uid, last_updated
        sql = "INSERT INTO list_role VALUES (NULL, %s, %s, %s, NULL)"
        # affected_rows = cursor.execute(sql, (roleInfo['role_name'], roleInfo['menu_available'], user_id))
        defaultRole = '1,2,3,4,5'
        affected_rows = cursor.execute(sql, (roleInfo['role_name'], defaultRole, user_id))
        conn.commit()
        cursor.close()
        if affected_rows != 0:
            return jsonify({"result": "success"})
        else:
            return jsonify({"result": "Not have affected"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/deleteRole', methods=['POST'])
def deleteRole():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)
        role = dataInput['role']
        role_id = str(role['role_id'])

        '''
        # IDEA:
            if owner is root = can delete admin, user
            if owner is admin = can delete user only owner
        '''

        if lvl == '1' or lvl == '2': # Root or Admin
            list_role_affected_rows, pre_add_role_affected_rows, user_list_role_affected_rows = deleteARole(user_id, role_id)
            if list_role_affected_rows != 0 or pre_add_role_affected_rows != 0 or user_list_role_affected_rows != 0 :
                return jsonify({"result": "success"})
            else:
                return jsonify({"result": "Not found your role"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/deleteRoleMore', methods=['POST'])
def deleteRoleMore():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        roles_id = dataInput['targetRoleID'] # expect: ['1', '2', '3', '4']

        '''
        # IDEA:
            if owner is root = can delete admin, user
            if owner is admin = can delete user only owner
        '''

        if lvl == '1' or lvl == '2': # Root or Admin
            for role_id in roles_id:
                list_role_affected_rows, pre_add_role_affected_rows, user_list_role_affected_rows = deleteARole(user_id, role_id)
            return jsonify({"result": "success"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/editRole', methods=['POST'])
def editRole():
    try:
        dataInput = request.json
        user = dataInput['user'] # email, firstname, lastname
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        roleInfo = dataInput['role'] # role_id, role_name, menu_available

        if lvl == '1' or lvl == '2': # Root or Admin
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT * FROM list_role WHERE owner_uid = %s AND role_id = %s"
            cursor.execute(sql, (str(user_id), str(roleInfo['role_id'])))
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            if len(result) != 0:
                # IDEA: แก้ไขชื่อ และคนในกลุ่ม ส่วน menu_available ก็อัพถ้ามี

                if 'menu_available' in roleInfo:
                    sql = "UPDATE list_role SET role_name = %s, menu_available = %s WHERE owner_uid = %s AND role_id = %s"
                    affected_rows = cursor.execute(sql, (str(roleInfo['role_name']), str(roleInfo['menu_available']), str(user_id), str(roleInfo['role_id'])))
                    conn.commit()
                    cursor.close()
                    return jsonify({"result": "success"})
                else:
                    sql = "UPDATE list_role SET role_name = %s WHERE owner_uid = %s AND role_id = %s"
                    affected_rows = cursor.execute(sql, (str(roleInfo['role_name']), str(user_id), str(roleInfo['role_id'])))
                    conn.commit()
                    cursor.close()
                    return jsonify({"result": "success"})
            else:
                conn.commit()
                cursor.close()
                return jsonify({"result": "Error: Not found your request role"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/editRoleMember', methods=['POST'])
def editRoleMember():
    try:
        dataInput = request.json
        user = dataInput['user'] # email, firstname, lastname
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)
        if lvl == '1' or lvl == '2': # Root or Admin
            roleInfo = dataInput['role'] # role_id, role_name, users_id
            return jsonify({"result": updateRoleUID(roleInfo, user_id)})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

@app.route('/mgmt/getRole', methods=['POST'])
def getRole():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)

        '''
        # IDEA:
            if owner is root = can select admin, user
            if owner is admin = can select user only owner
        '''

        # if lvl == '1': # Root
        #     conn = mysql.connect()
        #     cursor = conn.cursor()
        #     sql = "SELECT * FROM list_role"
        #     cursor.execute(sql)
        #     data = cursor.fetchall()
        #     columns = [column[0] for column in cursor.description]
        #     result = toJson(data, columns)
        #     conn.commit()
        #     cursor.close()
        #     if len(result) != 0 :
        #         return jsonify({"result": result})
        #     else:
        #         return jsonify({"result": "Not found user infomation"})

        # SELECT list_role.role_id, user_list_role.uid, list_role.role_name, list_role.menu_available, list_role.last_updated FROM user_list_role JOIN list_role ON user_list_role.role_id = list_role.role_id WHERE owner_uid = 16

        if lvl == '1' or lvl == '2': # Root or Admin
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT * FROM list_role WHERE owner_uid = %s"
            cursor.execute(sql, user_id)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)

            if len(result) != 0 :
                i = 0
                for row in result:
                    sql = "SELECT user.user_id, user.firstname, user.lastname FROM user WHERE user.user_id IN (SELECT uid FROM user_list_role WHERE role_id = %s)"
                    cursor.execute(sql, row['role_id'])
                    data = cursor.fetchall()
                    columns = [column[0] for column in cursor.description]
                    userInRole = toJson(data, columns)
                    result[i]['userInRole'] = userInRole
                    i = i + 1
                conn.commit()
                cursor.close()
                return jsonify({"result": result})
            else:
                conn.commit()
                cursor.close()
                return jsonify({"result": "Not found your role"})
        else:
            return jsonify({"result": "User can't access"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})

# THIS FOR USER TO OWN ROLE
@app.route('/mgmt/getOwnRoles', methods=['POST'])
def getOwnRoles():
    try:
        dataInput = request.json
        user = dataInput['user']
        email = user['email']
        lvl = getLevelUser(email)
        user_id = getUserID(user)
        if lvl == '3': # user only
            conn = mysql.connect()
            cursor = conn.cursor()
            sql = "SELECT list_role.menu_available FROM list_role WHERE role_id IN (SELECT role_id FROM user_list_role WHERE uid = " + str(user_id) + ")"
            cursor.execute(sql)
            data = cursor.fetchall()
            columns = [column[0] for column in cursor.description]
            result = toJson(data, columns)
            conn.commit()
            cursor.close()
            if len(result) != 0:
                menu = []
                for row in result:
                    menu += row['menu_available'].split(',')

                return jsonify({"result": list(set(menu))}) # Expect return: [] or ['1', '2', '3', '4']
            else:
                return jsonify({"result": []}) # Expect return: []
        else:
            return jsonify({"result": "Just for user level"})
    except Exception as e:
        current_app.logger.info(e)
        return jsonify({"result": "Error: " + str(e)})
