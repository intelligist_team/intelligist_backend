#!/usr/bin/python
#-*- coding: utf-8 -*-

import csv
import os
import codecs
import pymongo
import json

def makeObject(row):
    indexTemp = 1
    obj = {}
    obj['rowName'] = row[0]
    obj['MALE'] = row[1]
    obj['FEMALE'] = row[2]
    for idx, col in enumerate(row[3:203]):
        if idx % 2 == 0:
            obj['M'+str(indexTemp)] = row[idx+3]
        else:
            obj['F'+str(indexTemp)] = row[idx+3]
            indexTemp += 1
    obj['M>100'] = row[203]
    obj['F>100'] = row[204]
    obj['MALE_TDOB'] = row[205]
    obj['FEMALE_TDOB'] = row[206]
    obj['TOTAL_TDOB'] = row[207]
    obj['MALE_XHOUSE'] = row[208]
    obj['FEMALE_XHOUSE'] = row[209]
    obj['TOTAL_XHOUSE'] = row[210]
    obj['MALE_OTHER_NAT'] = row[211]
    obj['FEMALE_OTHER_NAT'] = row[212]
    obj['TOTAL_OTHER_NAT'] = row[213]
    obj['MALE_MOVE'] = row[214]
    obj['FEMALE_MOVE'] = row[215]
    obj['TOTAL_MOVE'] = row[216]
    obj['MALE_TOT'] = row[217]
    obj['FEMALE_TOT'] = row[218]
    obj['TOTAL_TOT'] = row[219]

    return obj

def batch_insert(listObj):
    # pass
    connection = pymongo.MongoClient('localhost', 27017)
    db = connection.populationsDB
    coll = db.by_age.insert_many(listObj)
    # coll.save({
    #
    # })
    print "insert!"

def clear_data():
    global table
    global data
    table = []
    data = {}
    data['province'] = {}
    data['amphoes'] = []
    data['tambons'] = []

def clear_tmpDataEachYear():
    global tmpDataEachYear
    tmpDataEachYear = []

def makeJSONfile(data_obj, file_output):
    # file_output must have '.json' notation e.g. 'data.json'
    # data_obj must be json obj
    with open(file_output, 'w') as outfile:
        json.dump(data_obj, outfile, ensure_ascii=False)

input_folder = './populations_data/byAge-csv/'

table = []

data = {}
data['province'] = {}
data['amphoes'] = []
data['tambons'] = []

tmpDataEachYear = []

if __name__ == "__main__":
    print "Doing..."

    # SET ZERO
    clear_data()

    for root, dirs, files in os.walk(input_folder):
        if files != [] :
            last_path = os.path.basename(os.path.normpath(root))
            print "do " + last_path + "..."
            for idx, file in enumerate(files):
                if file.endswith('.csv'):
                    filewithpath = os.path.join(root, file)
                    # print "do " + filewithpath

                    # OPEN FILE AND SAVE IN table (VARIABLE)
                    with codecs.open(filewithpath, 'rb') as csvfile:
                        fileReader = csv.reader(csvfile)
                        for row in fileReader:
                            table.append(row)

                    fileID = os.path.splitext(os.path.basename(filewithpath))[0]
                    data['provinceID'] = fileID # fileID from filename
                    data['year'] = last_path # year
                    for idx, row in enumerate(table):
                        if 'จังหวัด' in row[0] or 'กรุงเทพ' in row[0]:
                            data['province'] = makeObject(row)

                        elif 'อำเภอ' in row[0] or 'เขต' in row[0]:
                            tamObj = makeObject(row)
                            data['amphoes'].append(tamObj)

                        elif 'ตำบล' in row[0] or 'แขวง' in row[0]:
                            ampObj = makeObject(row)
                            data['tambons'].append(ampObj)

                    tmpDataEachYear.append(data)
                    clear_data()
            # makeJSONfile(tmpDataEachYear, 'data.json')
            batch_insert(tmpDataEachYear)
            clear_tmpDataEachYear()

    # makeJSONfile(data, 'data.json')
    # makeJSONfile(tmpDataEachYear, 'data.json')
    # print tmpDataEachYear
    print "Done!"
