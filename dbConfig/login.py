#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *
import uuid

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

@app.route('/login', methods=['POST'])
def login():
    dataInput = request.json
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT email, firstname, lastname, status, privilege_level FROM user WHERE email=%s AND password=%s"
    cursor.execute(sql,(dataInput['email'], dataInput['password']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if len(result) == 0:
        conn.commit()
        cursor.close()
        return jsonify({"status": 'not found'})
    else:
        if str(result[0]['status']) == '3':
            return 'Your email is suspended'
        elif str(result[0]['status']) == '4':
            return 'Please check in your email confirmation'
        elif str(result[0]['status']) == '5':
            return 'You are guest member.\nPlease contact administrator for more information'
        if str(result[0]['privilege_level']) == '1' or str(result[0]['privilege_level']) == '2': # Admin or Root
            result[0]['isAdmin'] = 'true'
        else:
            result[0]['isAdmin'] = 'false'
        sql = "UPDATE user SET status = 2 WHERE email = %s" # Update Status is Online
        cursor.execute(sql,(dataInput['email']))
        conn.commit()
        cursor.close()
        return jsonify({"status": "success", "data": result[0]})

@app.route('/logout', methods=['POST'])
def logout():
    dataInput = request.json
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT status FROM user WHERE email=%s"
    cursor.execute(sql, (str(dataInput['email'])))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    # if str(result[0]['status']) == '3':
    #     return jsonify({"status": "success"})

    sql = "UPDATE user SET status = 1 WHERE email = %s" # Update Status is Offline
    cursor.execute(sql, (str(dataInput['email'])))
    # data = cursor.fetchall()
    # columns = [column[0] for column in cursor.description]
    # result = toJson(data,columns)
    conn.commit()
    cursor.close()

    return jsonify({"status": "success"})

@app.route('/forgotPassword', methods=['POST'])
def forgotPassword():
    dataInput = request.json

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT email, firstname, lastname, status FROM user WHERE email=%s"
    cursor.execute(sql,(dataInput['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)
    if len(result) == 0:
        return 'not found'

    if str(result[0]['status']) == '3':
        return 'Your email is suspended'

    token = (str(uuid.uuid4()) + str(uuid.uuid1())).replace('-','')
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "INSERT INTO token_forgotpassword VALUES (NULL, %s, %s,%s,NULL)"
    cursor.execute(sql,(token, dataInput['email'], 'active'))
    conn.commit()
    cursor.close()

    sendMailResetPassword(token, dataInput['email'], dataInput['link'])
    conn.commit()
    cursor.close()
    return 'success'

# @app.route('/testSendMail', methods=['GET'])
def sendMailResetPassword(token, email, link):
    # token, email, link
    # link = 'http://localhost:8080'
    # token = 'klsakdfjlksdjflkj'
    # email = 'sirawit14@gmail.com'

    fromaddr = "Intelligist Platform <chana.su@intelligist.co.th>"
    toaddr = email
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Important - Reset Password Intelligist Platform"
    link = str(link) + "/resetpassword/" + str(token)
    footer ="<br><br><br>Team Intelligist Platform" + "<br>Call Center: +66 2 257 7000" + "<br>Email: chana.su@intelligist.co.th"
    body = "<p style='font-size: 14px;text-align:center'>Click below to reset your password</p> <a href='" + str(link) + "' style='margin: 0 auto;display: block;width: 160px;height: 60px;margin-top: 30px;background-color: #19b5fe;text-align: center;line-height: 60px;color: #ffffff;border-radius: 4px;text-decoration: none;'>Reset Password</a>" + footer

    msg.attach(MIMEText(body, 'html',"utf-8"))
    try:
        server = smtplib.SMTP('mailtx.inet.co.th', 25)
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        return "success"
    except:
       return "error"
    return 'success'

@app.route('/resetPassword', methods=['POST'])
def resetPassword():
    dataInput = request.json

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT email FROM token_forgotpassword WHERE token=%s"
    cursor.execute(sql,(dataInput['token']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "UPDATE token_forgotpassword SET is_active=%s WHERE token=%s"
    cursor.execute(sql,('inactive', dataInput['token']))
    conn.commit()
    cursor.close()

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "UPDATE user SET password=%s WHERE email=%s"
    cursor.execute(sql,(dataInput['password'], result[0]['email']))
    conn.commit()
    cursor.close()

    sendMailResetPasswordSuccess(result[0]['email'])
    return 'success'

def sendMailResetPasswordSuccess(email):
    fromaddr = "Intelligist Platform <chana.su@intelligist.co.th>"
    toaddr = email
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Reset Password Intelligist Platform Successful"
    footer ="<br><br><br>Team Intelligist Platform" + "<br>Call Center: +66 2 257 7000" + "<br>Email: chana.su@intelligist.co.th"
    body = "<p style='text-indent:40px;padding-right:40px;font-size: 14px;'>Reset password Intelligist Platform is successful, <br>You can log in with your Email and your new password." + footer

    msg.attach(MIMEText(body, 'html',"utf-8"))
    try:
        server = smtplib.SMTP('mailtx.inet.co.th', 25)
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        return "success"
    except:
       return "error"
    return 'success'

@app.route('/validateEmail', methods=['POST'])
def validateEmail():
    # expect {email, firstname, lastname}
    dataInput = request.json

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT firstname, lastname FROM user WHERE email=%s"
    cursor.execute(sql,(dataInput['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)
    conn.commit()
    cursor.close()

    if len(result) != 0 :
        if (dataInput['firstname'] == result[0]['firstname'] and dataInput['lastname'] == result[0]['lastname']):
            return jsonify({"status": "success"})
        else:
            return jsonify({"status": "not found"})
    else:
        return jsonify({"status": "not found"})
