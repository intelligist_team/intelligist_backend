#!/usr/bin/python
#-*- coding: utf-8 -*-

import csv
import os
import codecs
import pymongo
import json

def makeObject(row):
    obj = {}
    obj['rowName'] = row[0]
    obj['MALE'] = row[1]
    obj['FEMALE'] = row[2]
    obj['TOTAL'] = row[3]
    obj['HOUSE'] = row[4]

    return obj

def batch_insert(listObj):
    global connection
    db = connection.populationsDB
    coll = db.by_year.insert_many(listObj)
    print "insert!"

def clear_data():
    global table
    global data
    global dataFirst
    table = []
    data = {}
    data['province'] = {}
    data['amphoes'] = []
    data['tambons'] = []
    dataFirst = {}

def clear_tmpDataEachYear():
    global tmpDataEachYear
    tmpDataEachYear = []

def makeJSONfile(data_obj, file_output):
    # file_output must have '.json' notation e.g. 'data.json'
    # data_obj must be json obj
    with open(file_output, 'w') as outfile:
        json.dump(data_obj, outfile, ensure_ascii=False)

input_folder = './populations_data/byYear-csv/'

connection = pymongo.MongoClient('localhost', 27017)

table = []

data = {}
data['province'] = {}
data['amphoes'] = []
data['tambons'] = []
dataFirst = {}
tmpDataEachYear = []

if __name__ == "__main__":
    print "Start..."

    # SET ZERO
    clear_data()
    clear_tmpDataEachYear()

    for root, dirs, files in os.walk(input_folder):
        if files != [] :
            last_path = os.path.basename(os.path.normpath(root))
            print "do " + last_path + "..."
            for idx_file, file in enumerate(files):
                if file.endswith('.csv'):
                    filewithpath = os.path.join(root, file)
                    print "do " + filewithpath

                    # OPEN FILE AND SAVE IN table (VARIABLE)
                    with codecs.open(filewithpath, 'rb') as csvfile:
                        fileReader = csv.reader(csvfile)
                        for row in fileReader:
                            table.append(row)

                    countProvince = 0
                    flag = False

                    fileID = os.path.splitext(os.path.basename(filewithpath))[0]
                    data['year'] = "25"+fileID[-2:]
                    for idx_row, row in enumerate(table):
                        if 'ทั่วประเทศ' in row[0]: # ทั่วประเทศ
                            countProvince += 1
                            provinceID = fileID+"_%02d" % countProvince # fileID from filename
                            dataFirst['provinceID'] = provinceID
                            dataFirst['year'] = "25"+fileID[-2:]
                            dataFirst['province'] = makeObject(row)
                            tmpDataEachYear.append(dataFirst)
                        elif 'ทั่วราชอาณาจักร' in row[0]:
                            countProvince += 1
                            provinceID = fileID+"_%02d" % countProvince # fileID from filename
                            row[0] = 'ทั่วประเทศ'
                            dataFirst['provinceID'] = provinceID
                            dataFirst['year'] = "25"+fileID[-2:]
                            dataFirst['province'] = makeObject(row)
                            tmpDataEachYear.append(dataFirst)
                        else:
                            if 'จังหวัด' in row[0] or 'กรุงเทพ' in row[0]:
                                if flag:
                                    tmpDataEachYear.append(data)
                                    clear_data()
                                countProvince += 1
                                provinceID = fileID+"_%02d" % countProvince # fileID from filename
                                data['provinceID'] = provinceID
                                data['year'] = "25"+fileID[-2:]
                                data['province'] = makeObject(row)
                                flag = True
                            elif 'อำเภอ' in row[0] or 'เขต' in row[0]:
                                tamObj = makeObject(row)
                                data['amphoes'].append(tamObj)
                            elif 'ตำบล' in row[0] or 'แขวง' in row[0]:
                                ampObj = makeObject(row)
                                data['tambons'].append(ampObj)
                    # last insert
                    tmpDataEachYear.append(data)

                    # final of file: insert to db
                    batch_insert(tmpDataEachYear)
                    clear_data()
                    clear_tmpDataEachYear()

    print "Finished!"
