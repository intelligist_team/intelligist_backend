# -*- coding: utf-8 -*-
#!/usr/bin/env python

from dbConfig import *
from StringIO import StringIO
import xlrd
import datetime
from collections import OrderedDict

import codecs
import csv

try:
    import xml.etree.cElementTree as xmlet
except ImportError:
    import xml.etree.ElementTree as xmlet

reload(sys)
sys.setdefaultencoding('utf8')

# ===============
# ==== MySQL ====
# ===============

def insertMySQL(userID, srcID, append_mode, filename, headername):
    fileInfo = str(filename).split('.')[0].replace('U', '').split('_')
    userID = fileInfo[0] if userID != '' else userID
    srcID = fileInfo[1] if srcID != '' else srcID
    # tableName = str(srcID)

    # current_app.logger.info(append_mode) # 1 is append, 0 is replace/update
    # current_app.logger.info(headername) # headerName

    try:
        conn = mysqlDB.get_db()
        cursor = conn.cursor()
        try:
            sql = "USE U" + str(userID)
            cursor.execute(sql)
        except Exception as e:
            sql = "CREATE DATABASE IF NOT EXISTS U" + str(userID)
            cursor.execute(sql)
            sql = "USE U" + str(userID)
            cursor.execute(sql)

        filewithpath = os.path.join(UPLOAD_PATH + str(userID) + '/', filename)
        fileExtension = getFileExtension(filename)

        # <<<<<<<<<<<<<<<<<
        HAS_EXT_HEADER = True if headername != "" else False
        APPEND_MODE = True if str(append_mode) == "1" else False
        # >>>>>>>>>>>>>>>>>

        # if fileExtension == "xls" or fileExtension == "xlsx" or fileExtension == "xlsm":
        #     try:
        #         fid = str(userID) + str(srcID)
        #         fileOpener = open(filewithpath, 'rb')
        #         file_id = fs.put(fileOpener.read(), filename=filename, fid=fid)
        #         return True
        #     except IOError as e:
        #         current_app.logger.info('this IO:', e)
        #         return False
        #     except Exception as f:
        #         current_app.logger.info(f)
        #         return False
        # elif fileExtension == "csv":
        #     try:
        #         fid = str(userID) + str(srcID)
        #         fileOpener = open(filewithpath, 'rb')
        #         file_id = fs.put(fileOpener.read(), filename=filename, fid=fid)
        #
        #         return True
        #     except IOError as e:
        #         current_app.logger.info('this IO:', e)
        #         return False
        #     except Exception as f:
        #         current_app.logger.info(f)
        #         return False
        # elif fileExtension == "txt":
        #     pass
        # elif fileExtension == "xml":
        #     try:
        #         fid = str(userID) + str(srcID)
        #         fileOpener = open(filewithpath, 'rb')
        #         file_id = fs.put(fileOpener.read(), filename=filename, fid=fid)
        #         return True
        #     except IOError as e:
        #         current_app.logger.info('this IO:', e)
        #         return False
        #     except Exception as f:
        #         current_app.logger.info(f)
        #         return False
        # else:
        #     return False

        fileOpener = open(filewithpath, 'rb')

        if fileExtension == "xls" or fileExtension == "xlsx" or fileExtension == "xlsm":
            try:
                # NOW IS OPENPYXL IS NOT SUPPORTED XLS EXTENSION EXCEPT xlrd (lib)

                book = xlrd.open_workbook(file_contents=fileOpener.read())
                # current_app.logger.info("The number of worksheets is {0}".format(book.nsheets) + "\nWorksheet name(s): {0}".format(book.sheet_names()))
                # content = []
                # chunkData = {}
                for wsx in range(book.nsheets):
                    sh = book.sheet_by_index(wsx)
                    # current_app.logger.info("Worksheet: {0}".format(wsx+1) + "\nname: {0} \nrow: {1} \ncol: {2}".format(sh.name, sh.nrows, sh.ncols))
                    # table = []
                    if sh.nrows != 0:
                        eachQuery = []
                        inputColumnName = []
                        for rx in range(sh.nrows):
                            if rx == 0:
                                # table.append([str(item) for item in sh.row_values(rowx=rx)])

                                tableName = str(srcID) + '_' + str(str(wsx+1).zfill(3))

                                # DEBUG: DROP First Insert Later
                                # sqlDelete = "DROP TABLE `" + tableName + "`"
                                # cursor.execute(sqlDelete)
                                # conn.commit()

                                sqlCreate = "CREATE TABLE IF NOT EXISTS `" + tableName + "` (id int NOT NULL AUTO_INCREMENT"
                                inputColumnName.append("id")
                                i = 1
                                if HAS_EXT_HEADER == True: # upload external header, row is excel format, add all of data
                                    try:
                                        fileHeaderwithpath = os.path.join(UPLOAD_PATH + str(userID) + '/', headername)
                                        fileHeaderExtension = getFileExtension(headername)
                                        fileHeaderOpener = open(fileHeaderwithpath, 'rb')
                                        bookHeader = xlrd.open_workbook(file_contents=fileHeaderOpener.read())
                                        sheetHeader = bookHeader.sheet_by_index(wsx)
                                        firstRow = sheetHeader.row_values(rowx=0)
                                        cleanFirstRow = notDupEver(firstRow)
                                        for item in cleanFirstRow:
                                            itemStr = str(item).encode('string_escape')
                                            if isAscii(item):
                                                if item == '':
                                                    sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                                    inputColumnName.append("COLUMN_" + excel_col(i))
                                                else:
                                                    sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                                                    inputColumnName.append(itemStr)
                                            else:
                                                sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                                inputColumnName.append("COLUMN_" + excel_col(i))
                                            i += 1
                                        listString = "(NULL, " + ", ".join(["'" + str(item).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'") + "'" if sh.cell_type(rowx = rx, colx = cx) != 3 else "'" + str(datetime.datetime(*xlrd.xldate_as_tuple(item, book.datemode)).strftime("%Y-%m-%d %H:%M:%S")).encode('utf-8').decode('string_escape').decode('utf-8') + "'" for cx, item in enumerate(sh.row_values(rowx=rx))]) + ")"
                                        eachQuery.append(listString)
                                    except Exception as e:
                                        return "Error: Header file error in sheet name is '" + str(sh.name) + "'"
                                else:
                                    firstRow = sh.row_values(rowx=0)
                                    cleanFirstRow = notDupEver(firstRow)
                                    for item in cleanFirstRow:
                                        itemStr = str(item).encode('string_escape')
                                        if isAscii(item):
                                            if item == '':
                                                sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                                inputColumnName.append("COLUMN_" + excel_col(i))
                                            else:
                                                sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                                                inputColumnName.append(itemStr)
                                        else:
                                            sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                            inputColumnName.append("COLUMN_" + excel_col(i))
                                        i += 1
                                sqlCreate += ", PRIMARY KEY (id))"
                            else:
                                # table.append([str(item) for item in sh.row_values(rowx=rx)])
                                listString = "(NULL, " + ", ".join(["'" + str(item).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'") + "'" if sh.cell_type(rowx = rx, colx = cx) != 3 else "'" + str(datetime.datetime(*xlrd.xldate_as_tuple(item, book.datemode)).strftime("%Y-%m-%d %H:%M:%S")).encode('utf-8').decode('string_escape').decode('utf-8') + "'" for cx, item in enumerate(sh.row_values(rowx=rx))]) + ")"
                                eachQuery.append(listString)

                        sqlCheckTableName = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' and TABLE_SCHEMA = 'U" + str(userID) + "'"
                        cursor.execute(sqlCheckTableName)
                        colName = cursor.fetchall()
                        columns = [column[0] for column in cursor.description]
                        jsonColumnName = toJson(colName, columns)
                        oldColumnName = [str(obj['COLUMN_NAME']) for obj in jsonColumnName]
                        # current_app.logger.info(oldColumnName)
                        # current_app.logger.info(inputColumnName)

                        foundChangeName = False
                        if len(inputColumnName) == len(oldColumnName):
                            sqlRename = "ALTER TABLE `" + tableName + "` "
                            changeNameQuery = []
                            for old, new in zip(oldColumnName, inputColumnName):
                                if old != new:
                                    changeNameQuery.append("CHANGE `" + old + "` `" + new + "` text")
                                    foundChangeName = True
                            if foundChangeName:
                                sqlRename += ", ".join(changeNameQuery)
                                cursor.execute(sqlRename)
                                conn.commit()
                                oldColumnName = inputColumnName
                                foundChangeName = False

                        sqlInsert = "INSERT INTO `" + tableName + "` VALUES "
                        if (inputColumnName != oldColumnName and len(oldColumnName) != 0):
                            return "Error: column are not match"
                        else:
                            try:
                                cursor.execute(sqlCreate)
                                conn.commit()
                            except Exception as e:
                                pass
                                # current_app.logger.info(str(e))

                            sqlInsert += ", ".join(eachQuery)
                            try:
                                if APPEND_MODE == True:
                                    # ADD IMMEDIATELY
                                    cursor.execute(sqlInsert)
                                    conn.commit()
                                else:
                                    # DELETE DATA FIRST
                                    sqlDelete = "TRUNCATE TABLE `" + tableName + "`"
                                    cursor.execute(sqlDelete)
                                    conn.commit()
                                    # ADD DATA LATER
                                    cursor.execute(sqlInsert)
                                    conn.commit()
                            except Exception as e:
                                return "Error: Inserting data to database error"

                cursor.close()
                fileOpener.close()
                return "success"
                # return jsonify({"result": chunkData})
            except IOError as e:
                cursor.close()
                fileOpener.close()
                current_app.logger.info('this IO: ', e)
                return "Error: " + str(e)
                # return False
            except Exception as f:
                cursor.close()
                fileOpener.close()
                current_app.logger.info(f)
                return "Error: " + str(f)
                # return False
        elif fileExtension == "csv":
            # OPEN FILE AND SAVE IN table (VARIABLE)
            try:
                # table = []
                tableName = str(srcID)

                # DEBUG: DROP First Insert Later
                # sqlDelete = "DROP TABLE `" + tableName + "`"
                # cursor.execute(sqlDelete)
                # conn.commit()

                eachQuery = []
                fileReader = csv.reader(StringIO(fileOpener.read()))
                inputColumnName = []
                for rx, row in enumerate(fileReader):
                    if rx == 0:
                        # table.append([str(item) for item in sh.row_values(rowx=rx)])
                        sqlCreate = "CREATE TABLE IF NOT EXISTS `" + tableName + "` (id int NOT NULL AUTO_INCREMENT"
                        inputColumnName.append("id")
                        i = 1
                        if HAS_EXT_HEADER == True: # upload external header, row is excel format, add all of data
                            try:
                                fileHeaderwithpath = os.path.join(UPLOAD_PATH + str(userID) + '/', headername)
                                fileHeaderExtension = getFileExtension(headername)
                                fileHeaderOpener = open(fileHeaderwithpath, 'rb')
                                fileHeaderReader = csv.reader(StringIO(fileHeaderOpener.read()))
                                for rxHeader, rowHeader in enumerate(fileHeaderReader):
                                    if rxHeader == 0:
                                        cleanFirstRow = notDupEver(rowHeader)
                                        for item in cleanFirstRow:
                                            itemStr = str(item).encode('string_escape')
                                            if isAscii(item):
                                                if item == '':
                                                    sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                                    inputColumnName.append("COLUMN_" + excel_col(i))
                                                else:
                                                    sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                                                    inputColumnName.append(itemStr)
                                            else:
                                                sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                                inputColumnName.append("COLUMN_" + excel_col(i))
                                            i += 1
                                listRow = ["'" + str(item).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "'" for item in row]
                                while len(listRow) < len(inputColumnName) - 1:
                                    # current_app.logger.info("I Catch You! Empty String")
                                    listRow.append("''")
                                listString = "(NULL, " + ", ".join(listRow) + ")"
                                eachQuery.append(listString)
                            except Exception as e:
                                return "Error: Header file error in '" + str(headername) + "'"
                        else:
                            cleanEachRow = notDupEver(row)
                            if cleanEachRow == "Duplicate Column":
                                return "Error: " + cleanEachRow
                            for item in cleanEachRow:
                                itemStr = str(item).encode('string_escape')
                                if isAscii(item):
                                    if item == '':
                                        sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                        inputColumnName.append("COLUMN_" + excel_col(i))
                                    else:
                                        sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                                        inputColumnName.append(itemStr)
                                else:
                                    sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                    inputColumnName.append("COLUMN_" + excel_col(i))
                                i += 1
                        sqlCreate += ", PRIMARY KEY (id))"
                    else:
                        # table.append([str(item) for item in sh.row_values(rowx=rx)])
                        # listString = "(NULL, " + ", ".join(["'" + str(item).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'") + "'" if sh.cell_type(rowx = rx, colx = cx) != 3 else "'" + str(datetime.datetime(*xlrd.xldate_as_tuple(item, book.datemode)).strftime("%Y-%m-%d %H:%M:%S")).encode('utf-8').decode('string_escape').decode('utf-8') + "'" for cx, item in enumerate(sh.row_values(rowx=rx))]) + ")"
                        listRow = ["'" + str(item).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "'" for item in row]
                        while len(listRow) < len(inputColumnName) - 1:
                            # current_app.logger.info("I Catch You! Empty String")
                            listRow.append("''")
                        listString = "(NULL, " + ", ".join(listRow) + ")"
                        eachQuery.append(listString)

                sqlCheckTableName = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' and TABLE_SCHEMA = 'U" + str(userID) + "'"
                cursor.execute(sqlCheckTableName)
                colName = cursor.fetchall()
                columns = [column[0] for column in cursor.description]
                jsonColumnName = toJson(colName, columns)
                oldColumnName = [str(obj['COLUMN_NAME']) for obj in jsonColumnName]
                # current_app.logger.info(oldColumnName)
                # current_app.logger.info(inputColumnName)

                foundChangeName = False
                if len(inputColumnName) == len(oldColumnName):
                    sqlRename = "ALTER TABLE `" + tableName + "` "
                    changeNameQuery = []
                    for old, new in zip(oldColumnName, inputColumnName):
                        if old != new:
                            changeNameQuery.append("CHANGE `" + old + "` `" + new + "` text")
                            foundChangeName = True
                    if foundChangeName:
                        sqlRename += ", ".join(changeNameQuery)
                        cursor.execute(sqlRename)
                        conn.commit()
                        oldColumnName = inputColumnName
                        foundChangeName = False

                sqlInsert = "INSERT INTO `" + tableName + "` VALUES "
                if (inputColumnName != oldColumnName and len(oldColumnName) != 0):
                    return "Error: column are not match"
                else:
                    try:
                        cursor.execute(sqlCreate)
                        conn.commit()
                    except Exception as e:
                        current_app.logger.info(str(e))

                    sqlInsert += ", ".join(eachQuery)
                    if APPEND_MODE == True:
                        # ADD IMMEDIATELY
                        cursor.execute(sqlInsert)
                        conn.commit()
                    else:
                        # DELETE DATA FIRST
                        sqlDelete = "TRUNCATE TABLE `" + tableName + "`"
                        cursor.execute(sqlDelete)
                        conn.commit()
                        # ADD DATA LATER
                        cursor.execute(sqlInsert)
                        conn.commit()
                fileOpener.close()
                cursor.close()
                return "success"
            except IOError as e:
                cursor.close()
                fileOpener.close()
                current_app.logger.info('this IO:', e)
                return "Error: " + str(e)
            except Exception as f:
                cursor.close()
                fileOpener.close()
                current_app.logger.info(f)
                return "Error: " + str(f)
        elif fileExtension == "txt":
            try:
                # IDEA: ***** just open txt in csv reader and do like csv format *****
                fileReader = csv.reader(fileOpener, delimiter='|')

                # test = csvDownloader(fileReader, str(srcID))
                # watprint(test)

                # with open(os.path.join(UPLOAD_PATH + str(userID) + '/', 'tmp'), 'wb') as output_file:
                #     out_csv.writerows(in_txt)

                # for rx, row in enumerate(in_txt):
                #     current_app.logger.info(row)

                '''<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>'''
                '''<<<<<<<<<<<< CODE LIKE THE CSV FORMAT >>>>>>>>>>>>'''
                '''<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>'''
                # แก้ตรง csv reader ทั้ง file, headerfile

                tableName = str(srcID)

                eachQuery = []
                inputColumnName = []
                for rx, row in enumerate(fileReader):
                    if rx == 0:
                        sqlCreate = "CREATE TABLE IF NOT EXISTS `" + tableName + "` (id int NOT NULL AUTO_INCREMENT"
                        inputColumnName.append("id")
                        i = 1
                        if HAS_EXT_HEADER == True: # upload external header, row is excel format, add all of data
                            try:
                                fileHeaderwithpath = os.path.join(UPLOAD_PATH + str(userID) + '/', headername)
                                fileHeaderExtension = getFileExtension(headername)
                                fileHeaderOpener = open(fileHeaderwithpath, 'rb')
                                fileHeaderReader = csv.reader(fileHeaderOpener, delimiter='|')
                                for rxHeader, rowHeader in enumerate(fileHeaderReader):
                                    if rxHeader == 0:
                                        cleanFirstRow = notDupEver(rowHeader)
                                        for item in cleanFirstRow:
                                            itemStr = str(item).encode('string_escape')
                                            if isAscii(item):
                                                if item == '':
                                                    sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                                    inputColumnName.append("COLUMN_" + excel_col(i))
                                                else:
                                                    sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                                                    inputColumnName.append(itemStr)
                                            else:
                                                sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                                inputColumnName.append("COLUMN_" + excel_col(i))
                                            i += 1
                                listRow = ["'" + str(item).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "'" for item in row]
                                while len(listRow) < len(inputColumnName) - 1:
                                    # current_app.logger.info("I Catch You! Empty String")
                                    listRow.append("''")
                                listString = "(NULL, " + ", ".join(listRow) + ")"
                                eachQuery.append(listString)
                            except Exception as e:
                                return "Error: Header file error in '" + str(headername) + "'"
                        else:
                            cleanEachRow = notDupEver(row)
                            if cleanEachRow == "Duplicate Column":
                                return "Error: " + cleanEachRow
                            for item in cleanEachRow:
                                itemStr = str(item).encode('string_escape')
                                if isAscii(item):
                                    if item == '':
                                        sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                        inputColumnName.append("COLUMN_" + excel_col(i))
                                    else:
                                        sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                                        inputColumnName.append(itemStr)
                                else:
                                    sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                    inputColumnName.append("COLUMN_" + excel_col(i))
                                i += 1
                        sqlCreate += ", PRIMARY KEY (id))"
                    else:
                        # table.append([str(item) for item in sh.row_values(rowx=rx)])
                        # listString = "(NULL, " + ", ".join(["'" + str(item).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'") + "'" if sh.cell_type(rowx = rx, colx = cx) != 3 else "'" + str(datetime.datetime(*xlrd.xldate_as_tuple(item, book.datemode)).strftime("%Y-%m-%d %H:%M:%S")).encode('utf-8').decode('string_escape').decode('utf-8') + "'" for cx, item in enumerate(sh.row_values(rowx=rx))]) + ")"
                        listRow = ["'" + str(item).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "'" for item in row]
                        while len(listRow) < len(inputColumnName) - 1:
                            # current_app.logger.info("I Catch You! Empty String")
                            listRow.append("''")
                        listString = "(NULL, " + ", ".join(listRow) + ")"
                        eachQuery.append(listString)

                sqlCheckTableName = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' and TABLE_SCHEMA = 'U" + str(userID) + "'"
                cursor.execute(sqlCheckTableName)
                colName = cursor.fetchall()
                columns = [column[0] for column in cursor.description]
                jsonColumnName = toJson(colName, columns)
                oldColumnName = [str(obj['COLUMN_NAME']) for obj in jsonColumnName]
                # current_app.logger.info(oldColumnName)
                # current_app.logger.info(inputColumnName)

                foundChangeName = False
                if len(inputColumnName) == len(oldColumnName):
                    sqlRename = "ALTER TABLE `" + tableName + "` "
                    changeNameQuery = []
                    for old, new in zip(oldColumnName, inputColumnName):
                        if old != new:
                            changeNameQuery.append("CHANGE `" + old + "` `" + new + "` text")
                            foundChangeName = True
                    if foundChangeName:
                        sqlRename += ", ".join(changeNameQuery)
                        cursor.execute(sqlRename)
                        conn.commit()
                        oldColumnName = inputColumnName
                        foundChangeName = False

                sqlInsert = "INSERT INTO `" + tableName + "` VALUES "
                if (inputColumnName != oldColumnName and len(oldColumnName) != 0):
                    return "Error: column are not match"
                else:
                    try:
                        cursor.execute(sqlCreate)
                        conn.commit()
                    except Exception as e:
                        current_app.logger.info(str(e))

                    sqlInsert += ", ".join(eachQuery)
                    if APPEND_MODE == True:
                        # ADD IMMEDIATELY
                        cursor.execute(sqlInsert)
                        conn.commit()
                    else:
                        # DELETE DATA FIRST
                        sqlDelete = "TRUNCATE TABLE `" + tableName + "`"
                        cursor.execute(sqlDelete)
                        conn.commit()
                        # ADD DATA LATER
                        cursor.execute(sqlInsert)
                        conn.commit()

                '''<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>'''
                '''<<<<<<<<<<<< END CODE LIKE THE CSV FORMAT >>>>>>>>>>>>'''
                '''<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>'''

                cursor.close()
                fileOpener.close()
                return "success"
            except IOError as e:
                cursor.close()
                fileOpener.close()
                current_app.logger.info('this IO:', e)
                return "Error: " + str(e)
            except Exception as f:
                cursor.close()
                fileOpener.close()
                current_app.logger.info(f)
                return "Error: " + str(f)
        elif fileExtension == "xml":
            try:
                tableName = str(srcID)

                # DEBUG: DROP First Insert Later
                # sqlDelete = "DROP TABLE `" + tableName + "`"
                # cursor.execute(sqlDelete)
                # conn.commit()

                widthSize = 0
                heightSize = 0
                header = OrderedDict()

                for event, elem in xmlet.iterparse(StringIO(fileOpener.read())):
                    parent = elem.tag

                    for idx, val in enumerate(elem):
                        child = val.tag
                        child_text = val.text

                        if len(val) == 0: # last child node
                            try:
                                header[parent+'_'+str(idx+1)+'_'+child].append(child_text)
                            except KeyError:
                                header[parent+'_'+str(idx+1)+'_'+child] = []
                                header[parent+'_'+str(idx+1)+'_'+child].append(child_text)

                widthSize = len(header)
                for v in header.itervalues():
                    heightSize = len(v) if len(v) >= heightSize else heightSize

                # current_app.logger.info(str(widthSize) + 'x' + str(heightSize))
                headerSorted = sorted(header, key=lambda x: len(header[x]))

                inputColumnName = []
                sqlCreate = "CREATE TABLE IF NOT EXISTS `" + tableName + "` (id int NOT NULL AUTO_INCREMENT"
                inputColumnName.append("id")

                preHeader = ["" + str('_'.join(eachHead.split('_')[2:])).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "" for eachHead in headerSorted]
                cleanHeader = notDupEver(preHeader)
                if cleanHeader == "Duplicate Column":
                    return "Error: " + cleanHeader
                for idx, item in enumerate(cleanHeader):
                    itemStr = str(item).encode('string_escape')
                    if isAscii(item):
                        if item == '':
                            sqlCreate += ", COLUMN_" + excel_col(idx+1) + " TEXT NOT NULL "
                            inputColumnName.append("COLUMN_" + excel_col(idx+1))
                        else:
                            sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                            inputColumnName.append(itemStr)
                    else:
                        sqlCreate += ", COLUMN_" + excel_col(idx+1) + " TEXT NOT NULL "
                        inputColumnName.append("COLUMN_" + excel_col(idx+1))

                # if HAS_EXT_HEADER == True:
                #     for idx, eachHead in enumerate(headerSorted):
                #         headerName = '_'.join(eachHead.split('_')[2:])
                #         sqlCreate += ", COLUMN_" + excel_col(idx+1) + " TEXT NOT NULL "
                #         inputColumnName.append("COLUMN_" + excel_col(idx+1))
                # else:
                #     preHeader = ["" + str('_'.join(eachHead.split('_')[2:])).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "" for eachHead in headerSorted]
                #     cleanHeader = notDupEver(preHeader)
                #     if cleanHeader == "Duplicate Column":
                #         return "Error: " + cleanHeader
                #     for idx, item in enumerate(cleanHeader):
                #         itemStr = str(item).encode('string_escape')
                #         if isAscii(item):
                #             if item == '':
                #                 sqlCreate += ", COLUMN_" + excel_col(idx+1) + " TEXT NOT NULL "
                #                 inputColumnName.append("COLUMN_" + excel_col(idx+1))
                #             else:
                #                 sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                #                 inputColumnName.append(itemStr)
                #         else:
                #             sqlCreate += ", COLUMN_" + excel_col(idx+1) + " TEXT NOT NULL "
                #             inputColumnName.append("COLUMN_" + excel_col(idx+1))

                sqlCreate += ", PRIMARY KEY (id))"

                newHeader = OrderedDict()
                firstRowHeader = []
                for head in headerSorted:
                    newHeader[head] = header[head]
                    firstRowHeader.append(head.split('_', 2)[2])

                # FOR FULLFILL DATA TABLE WORKS:
                for key in newHeader:
                    lastRow = len(newHeader[key])-1
                    while len(newHeader[key]) < heightSize:
                        newHeader[key].append(newHeader[key][lastRow])

                # TRANSPOSE ARRAY
                table = []
                for k, v in newHeader.items():
                    table.append(v)
                table = zip(*table)

                # if HAS_EXT_HEADER == True:
                #     table.insert(0, firstRowHeader)
                eachQuery = []
                for rx, row in enumerate(table):
                    listString = "(NULL, " + ", ".join(["'" + str(item).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "'" for item in row]) + ")"
                    eachQuery.append(listString)

                sqlCheckTableName = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' and TABLE_SCHEMA = 'U" + str(userID) + "'"
                cursor.execute(sqlCheckTableName)
                colName = cursor.fetchall()
                columns = [column[0] for column in cursor.description]
                jsonColumnName = toJson(colName, columns)
                oldColumnName = [str(obj['COLUMN_NAME']) for obj in jsonColumnName]

                foundChangeName = False
                if len(inputColumnName) == len(oldColumnName):
                    sqlRename = "ALTER TABLE `" + tableName + "` "
                    changeNameQuery = []
                    for old, new in zip(oldColumnName, inputColumnName):
                        if old != new:
                            changeNameQuery.append("CHANGE `" + old + "` `" + new + "` text")
                            foundChangeName = True
                    if foundChangeName:
                        sqlRename += ", ".join(changeNameQuery)
                        cursor.execute(sqlRename)
                        conn.commit()
                        oldColumnName = inputColumnName
                        foundChangeName = False

                sqlInsert = "INSERT INTO `" + tableName + "` VALUES "
                if (inputColumnName != oldColumnName and len(oldColumnName) != 0):
                    return "Error: column are not match"
                else:
                    try:
                        cursor.execute(sqlCreate)
                        conn.commit()
                    except Exception as e:
                        current_app.logger.info(str(e))

                    sqlInsert += ", ".join(eachQuery)
                    if APPEND_MODE == True:
                        # ADD IMMEDIATELY
                        cursor.execute(sqlInsert)
                        conn.commit()
                    else:
                        # DELETE DATA FIRST
                        sqlDelete = "TRUNCATE TABLE `" + tableName + "`"
                        cursor.execute(sqlDelete)
                        conn.commit()
                        # ADD DATA LATER
                        cursor.execute(sqlInsert)
                        conn.commit()
                cursor.close()
                fileOpener.close()
                return "success"
            except IOError as e:
                cursor.close()
                fileOpener.close()
                current_app.logger.info('this IO:', e)
                return "Error: " + str(e)
            except Exception as f:
                cursor.close()
                fileOpener.close()
                current_app.logger.info(f)
                return "Error: " + str(f)
                # return False
    except Exception as e:
        current_app.logger.info(str(e))
        return "Error: " + str(e)

    # elif fileExtension == "xml":
    #         try:
    #             widthSize = 0
    #             heightSize = 0
    #             header = OrderedDict()
    #
    #             for event, elem in xmlet.iterparse(StringIO(fileOpener.read())):
    #                 parent = elem.tag
    #
    #                 for idx, val in enumerate(elem):
    #                     child = val.tag
    #                     child_text = val.text
    #
    #                     if len(val) == 0: # last child node
    #                         try:
    #                             header[parent+'_'+str(idx+1)+'_'+child].append(child_text)
    #                         except KeyError:
    #                             header[parent+'_'+str(idx+1)+'_'+child] = []
    #                             header[parent+'_'+str(idx+1)+'_'+child].append(child_text)
    #
    #             widthSize = len(header)
    #             for v in header.itervalues():
    #                 heightSize = len(v) if len(v) >= heightSize else heightSize
    #
    #             # current_app.logger.info(str(widthSize) + 'x' + str(heightSize))
    #             headerSorted = sorted(header, key=lambda x: len(header[x]))
    #
    #             newHeader = OrderedDict()
    #             firstRowHeader = []
    #             for head in headerSorted:
    #                 newHeader[head] = header[head]
    #                 firstRowHeader.append(head.split('_', 2)[2])
    #
    #             # FOR FULLFILL DATA TABLE WORKS:
    #             for key in newHeader:
    #                 lastRow = len(newHeader[key])-1
    #                 while len(newHeader[key]) < heightSize:
    #                     newHeader[key].append(newHeader[key][lastRow])
    #
    #             # TRANSPOSE ARRAY
    #             table = []
    #             for k, v in newHeader.items():
    #                 table.append(v)
    #             table = zip(*table)
    #             table.insert(0, firstRowHeader)
    #
    #             content = makeObject(table)
    #             cursor.close()
    #             return jsonify({"result": content})
    #
    #         except IOError as e:
    #             cursor.close()
    #             current_app.logger.info('this IO:', e)
    #             return "Error: " + str(e)
    #         except Exception as f:
    #             cursor.close()
    #             current_app.logger.info(f)
    #             return "Error: " + str(f)
    #             # return False
    # except Exception as e:
    #     current_app.logger.info(str(e))
    #     return "Error: " + str(e)

def notDupEver(arr):
    try:
        cleanArr = sorted(set(arr), key=arr.index)
        k = 0
        for idx, val in enumerate(arr):
            if val == 'id':
                arr[idx] = 'COLUMN_' + excel_col(idx+1)

            try:
                if val == cleanArr[k]:
                    if val == '':
                        arr[idx] = 'COLUMN_' + excel_col(idx+1)
                    k = k + 1
                else:
                    arr[idx] = 'COLUMN_' + excel_col(idx+1)
            except IndexError:
                arr[idx] = 'COLUMN_' + excel_col(idx+1)
        return arr
    except Exception as e:
        current_app.logger.info(str(e))
        return "Duplicate Column"

def makeObject(arrayTable):
    dataObject = []
    keyHeader = []
    valueType = []
    chunkData = {}

    for idx, eachArr in enumerate(arrayTable):
        for idxInternal, value in enumerate(eachArr):
            inEachKey = []
            inEachKey.append(getDataType(value))
            inEachKey.append(value.decode("unicode_escape"))
            chunkData[excel_col(idxInternal)+'_'+str(idx+1)] = inEachKey
    # chunkData['ts'] = datetime.strptime(datetime.now(TZ).strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")
    dataObject.append(chunkData)
    return dataObject

'''
    IDEA:
    + get user object to identity own data
    + get source_id
'''
@app.route('/getDataFromMySQL', methods=['POST'])
def getDataFromMySQL():
    try:
        dataInput = request.json
        uid = str(getUserID(dataInput['user']))
        sid = str(dataInput['source_id'])

        conn = mysqlDB.get_db()
        cursor = conn.cursor()
        try:
            sql = "USE U" + str(uid)
            cursor.execute(sql)
        except Exception as e:
            return jsonify({"result": "Not found user's database"})

        sqlCheckTableName = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'U" + str(uid) + "' AND TABLE_NAME LIKE '" + sid + "%'"
        cursor.execute(sqlCheckTableName)
        fetTable = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        tableSQLdata = toJson(fetTable, columns)

        resultData = {}
        tableNameList = [sh['TABLE_NAME'] for sh in tableSQLdata]
        for idx, shName in enumerate(tableNameList):
            sqlEachTableName = "SELECT * FROM `" + shName + "` ORDER BY id"
            cursor.execute(sqlEachTableName)
            fetTable = cursor.fetchall()
            data = toArray(fetTable)

            sqlCheckColumnName = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + shName + "'"
            cursor.execute(sqlCheckColumnName)
            fetTable = cursor.fetchall()
            columnName = [val[0] for val in toArray(fetTable)]
            data.insert(0, columnName)
            resultData[shName] = data

        cursor.close()
        return jsonify({"result": resultData})
    except IOError as e:
        cursor.close()
        current_app.logger.info('this IO:', e)
        return "Error: " + str(e)
    except Exception as f:
        cursor.close()
        current_app.logger.info(f)
        return "Error: " + str(f)

def toArray(data):
    results = []
    for idx, row in enumerate(data):
        results.append(row)
    return results

def csvDownloader(fileReader, srcID):
    try:
        conn = mysqlDB.get_db()
        cursor = conn.cursor()

        tableName = str(srcID)

        # DEBUG: DROP First Insert Later
        # sqlDelete = "DROP TABLE `" + tableName + "`"
        # cursor.execute(sqlDelete)
        # conn.commit()

        eachQuery = []
        # fileReader = csvReader
        inputColumnName = []
        for rx, row in enumerate(fileReader):
            if rx == 0:
                # table.append([str(item) for item in sh.row_values(rowx=rx)])
                sqlCreate = "CREATE TABLE IF NOT EXISTS `" + tableName + "` (id int NOT NULL AUTO_INCREMENT"
                inputColumnName.append("id")
                i = 1
                if HAS_EXT_HEADER == True: # upload external header, row is excel format, add all of data
                    try:
                        fileHeaderwithpath = os.path.join(UPLOAD_PATH + str(userID) + '/', headername)
                        fileHeaderExtension = getFileExtension(headername)
                        fileHeaderOpener = open(fileHeaderwithpath, 'rb')
                        fileHeaderReader = csv.reader(StringIO(fileHeaderOpener.read()))
                        for rxHeader, rowHeader in enumerate(fileHeaderReader):
                            if rxHeader == 0:
                                cleanFirstRow = notDupEver(rowHeader)
                                for item in cleanFirstRow:
                                    itemStr = str(item).encode('string_escape')
                                    if isAscii(item):
                                        if item == '':
                                            sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                            inputColumnName.append("COLUMN_" + excel_col(i))
                                        else:
                                            sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                                            inputColumnName.append(itemStr)
                                    else:
                                        sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                        inputColumnName.append("COLUMN_" + excel_col(i))
                                    i += 1
                        listRow = ["'" + str(item).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "'" for item in row]
                        while len(listRow) < len(inputColumnName) - 1:
                            # current_app.logger.info("I Catch You! Empty String")
                            listRow.append("''")
                        listString = "(NULL, " + ", ".join(listRow) + ")"
                        eachQuery.append(listString)
                    except Exception as e:
                        return "Error: Header file error in '" + str(headername) + "'"
                else:
                    cleanEachRow = notDupEver(row)
                    if cleanEachRow == "Duplicate Column":
                        return "Error: " + cleanEachRow
                    for item in cleanEachRow:
                        itemStr = str(item).encode('string_escape')
                        if isAscii(item):
                            if item == '':
                                sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                                inputColumnName.append("COLUMN_" + excel_col(i))
                            else:
                                sqlCreate += ", `" + itemStr + "` TEXT NOT NULL "
                                inputColumnName.append(itemStr)
                        else:
                            sqlCreate += ", COLUMN_" + excel_col(i) + " TEXT NOT NULL "
                            inputColumnName.append("COLUMN_" + excel_col(i))
                        i += 1
                sqlCreate += ", PRIMARY KEY (id))"
            else:
                # table.append([str(item) for item in sh.row_values(rowx=rx)])
                # listString = "(NULL, " + ", ".join(["'" + str(item).encode('utf-8').decode('string_escape').decode('utf-8').replace('"', '\\"').replace("'", "\\'") + "'" if sh.cell_type(rowx = rx, colx = cx) != 3 else "'" + str(datetime.datetime(*xlrd.xldate_as_tuple(item, book.datemode)).strftime("%Y-%m-%d %H:%M:%S")).encode('utf-8').decode('string_escape').decode('utf-8') + "'" for cx, item in enumerate(sh.row_values(rowx=rx))]) + ")"
                listRow = ["'" + str(item).decode('string_escape').replace('"', '\\"').replace("'", "\\'") + "'" for item in row]
                while len(listRow) < len(inputColumnName) - 1:
                    # current_app.logger.info("I Catch You! Empty String")
                    listRow.append("''")
                listString = "(NULL, " + ", ".join(listRow) + ")"
                eachQuery.append(listString)

        sqlCheckTableName = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' and TABLE_SCHEMA = 'U" + str(userID) + "'"
        cursor.execute(sqlCheckTableName)
        colName = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        jsonColumnName = toJson(colName, columns)
        oldColumnName = [str(obj['COLUMN_NAME']) for obj in jsonColumnName]
        # current_app.logger.info(oldColumnName)
        # current_app.logger.info(inputColumnName)

        foundChangeName = False
        if len(inputColumnName) == len(oldColumnName):
            sqlRename = "ALTER TABLE `" + tableName + "` "
            changeNameQuery = []
            for old, new in zip(oldColumnName, inputColumnName):
                if old != new:
                    changeNameQuery.append("CHANGE `" + old + "` `" + new + "` text")
                    foundChangeName = True
            if foundChangeName:
                sqlRename += ", ".join(changeNameQuery)
                cursor.execute(sqlRename)
                conn.commit()
                oldColumnName = inputColumnName
                foundChangeName = False

        sqlInsert = "INSERT INTO `" + tableName + "` VALUES "
        if (inputColumnName != oldColumnName and len(oldColumnName) != 0):
            return "Error: column are not match"
        else:
            try:
                cursor.execute(sqlCreate)
                conn.commit()
            except Exception as e:
                current_app.logger.info(str(e))

            sqlInsert += ", ".join(eachQuery)
            if APPEND_MODE == True:
                # ADD IMMEDIATELY
                cursor.execute(sqlInsert)
                conn.commit()
            else:
                # DELETE DATA FIRST
                sqlDelete = "TRUNCATE TABLE `" + tableName + "`"
                cursor.execute(sqlDelete)
                conn.commit()
                # ADD DATA LATER
                cursor.execute(sqlInsert)
                conn.commit()
        cursor.close()
        return "success"
    except IOError as e:
        cursor.close()
        current_app.logger.info('this IO:', e)
        return "Error: " + str(e)
    except Exception as f:
        cursor.close()
        current_app.logger.info(f)
        return "Error: " + str(f)
