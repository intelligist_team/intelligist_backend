#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *
import sys, errno
from urllib import urlopen
# import operator
# from openpyxl import load_workbook

reload(sys)
sys.setdefaultencoding('utf8')

# ===============
# === MongoDB ===
# ===============

def importContent(userID, srcID, filename):
    filewithpath = os.path.join(UPLOAD_PATH + str(userID) + '/', filename)
    fileExtension = getFileExtension(filename)

    if fileExtension == "xls" or fileExtension == "xlsx" or fileExtension == "xlsm":
        try:
            fid = str(userID) + str(srcID)
            fileOpener = open(filewithpath, 'rb')
            file_id = fs.put(fileOpener.read(), filename=filename, fid=fid)
            fileOpener.close()
            return True
        except IOError as e:
            current_app.logger.info('this IO:', e)
            return False
        except Exception as f:
            current_app.logger.info(f)
            return False
    elif fileExtension == "csv":
        try:
            fid = str(userID) + str(srcID)
            fileOpener = open(filewithpath, 'rb')
            file_id = fs.put(fileOpener.read(), filename=filename, fid=fid)
            fileOpener.close()
            return True
        except IOError as e:
            current_app.logger.info('this IO:', e)
            return False
        except Exception as f:
            current_app.logger.info(f)
            return False
    elif fileExtension == "txt":
        pass
        # try:
        #     fid = str(userID) + str(srcID)
        #     fileOpener = open(filewithpath, 'rb')
        #     file_id = fs.put(fileOpener.read(), filename=filename, fid=fid)
        #     fileOpener.close()
        # except IOError as e:
        #     current_app.logger.info('this IO:', e)
        #     return False
        # except Exception as f:
        #     current_app.logger.info(f)
        #     return False
    elif fileExtension == "xml":
        try:
            fid = str(userID) + str(srcID)
            fileOpener = open(filewithpath, 'rb')
            file_id = fs.put(fileOpener.read(), filename=filename, fid=fid)
            fileOpener.close()
            return True
        except IOError as e:
            current_app.logger.info('this IO:', e)
            return False
        except Exception as f:
            current_app.logger.info(f)
            return False
    else:
        return False

def webImportContent(userID, srcID, webLink):
    # urlopen("webLink")
    # urlopen("http://online.effbot.org/rss.xml")
    return False

# def getUserCollection(userID, sourceID):
#     return 'U' + str(userID) + '_' + str(sourceID)

# def MongoInsert(userID, sourceID, listObj):
#     # user_collection = 'U' + str(userID) + '_' + str(sourceID) + '_' + str(getNowDatetime())
#     user_collection = getUserCollection(userID, sourceID)
#     _id = db[user_collection].insert(listObj)
#     if _id:
#         return True
#     else:
#         return False

'''
EXAMPLE>
FROM
    [
        ['City', 'Population in 2012', 'Population in 2011', 'Population in 2010'],
        ['string', 'number', 'number', 'number'],
        ['Milan', '1324110', '907563', '607906'],
        ['Naples', '959574', '1324110', '1137128'],
        ['Turin', '907563', '959574', '1080181'],
        ['Palermo', '655875', '371282', '907563'],
        ['Genoa', '607906', '580181', '707563'],
        ['Bologna', '380181', '655875', '480181'],
        ['Florence', '371282', '607906', '655875']
    ]

TO
    # version 1
    [
        {
            City: {
                type: string,
                value: Milan
            },
            Population in 2012: {
                type: number,
                value: 1324110
            },
            ...
        },
        ...
    ]

    # version 2
    [
        {
            A_1: {
                type: string,
                value: City
            }
        }
    ]

Example Usage MongoDB
    testObj = [{
        "key": "value",
        "key2": "value2"
        }]
    coll = db[u16].insert_many(testObj)

Example Usage openxlpy
    wb = load_workbook(filename = filewithpath)
    t = '~/intelligist_backend/uploads/' + str(userID) + '/' + filename
    current_app.logger.info(t)
    wb = load_workbook(filename=t, read_only=True)
    current_app.logger.info(wb.get_sheet_names())
    ws = wb[0]
    current_app.logger.info(ws)

    for row in ws.rows:
        for cell in row:
            current_app.logger.info(cell.value)
'''
