#!/usr/bin/python
#-*- coding: utf-8 -*-

print "\n=========================="
print "TXT to CSV Converter V1.00 (work with directory)"
print "Author: Chinnawat Chimdee"
print "=========================="
print "Command use: \n\tinsert INPUT Folder: <<directory>>"
print "Parameter <<directory>>"
print "use '.' (dot) as start point, it means the same directory of this file."
print "IO example: ./populations_data/byAge"
print "==========================\n"

import csv
import glob
import os
import codecs

directory = raw_input("INPUT Folder: ")
output = raw_input("OUTPUT Folder: ")
# directory = './populations_data/byAge/'
# output = './populations_data/byAge-csv/'
print "Doing..."

for root, dirs, files in os.walk(directory):
    if root[-3:] != 'old':
        for idx, file in enumerate(files):
            if file.endswith('.txt'):
                filewithpath = os.path.join(root, file)
                with codecs.open(filewithpath, "rb") as input_file:
                    in_txt = csv.reader(input_file, delimiter='|')
                    filename = os.path.splitext(os.path.basename(file))[0] + '.csv'

                    last_path = os.path.basename(os.path.normpath(root))
                    output_folder = os.path.join(output, last_path)

                    if not os.path.exists(output_folder):
                        os.makedirs(output_folder)

                    with open(os.path.join(output_folder, filename), 'wb') as output_file:
                        out_csv = csv.writer(output_file)
                        out_csv.writerows(in_txt)
                        print "Done!"
