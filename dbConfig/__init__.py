#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify, current_app, abort, send_from_directory, send_file
from flask_cors import CORS, cross_origin
from flaskext.mysql import MySQL

from datetime import datetime
import sys, errno
import os
import glob
import shutil
import pytz
import codecs
import string
import pymssql
import pymongo
import gridfs
import json
import csv
from bson.json_util import dumps

app = Flask(__name__)
app2 = Flask(__name__)

app.config['ALLOWED_EXTENSIONS'] = set(['xls', 'xlsm', 'xlsx', 'csv', 'txt', 'xml'])

# CORS(app, resources={r"/api/*": {"origins": ["http://203.154.58.142/*"]}})
CORS(app, origins=['http://203.154.58.142/*', 'http://localhost/*'])
CORS(app2, origins=['http://203.154.58.142/*', 'http://localhost/*'])
# CORS(app)
# CORS(app2)

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '@kla12131415@'
app.config['MYSQL_DATABASE_DB'] = 'intelligist'
app.config['MYSQL_DATABASE_HOST'] = '203.154.58.151'
mysql = MySQL()
mysql.init_app(app)

app2.config['MYSQL_DATABASE_USER'] = 'root'
app2.config['MYSQL_DATABASE_PASSWORD'] = '0l%53p1q1^WkXjF%'
app2.config['MYSQL_DATABASE_DB'] = 'default'
app2.config['MYSQL_DATABASE_HOST'] = '203.154.58.242'
mysqlDB = MySQL()
mysqlDB.init_app(app2)

# MSSQL_SERVER = "203.154.58.242"
# MSSQL_USER = "sa"
# MSSQL_PASSWORD = "POLL#Admin"
# MSSQL_DB = "master"

mongo = pymongo.MongoClient('203.154.58.151',
                            27017,
                            username='root',
                            password='@Kla12131415@',
                            authSource='admin',
                            authMechanism='SCRAM-SHA-1')
db = mongo.WorkbenchDB
fs = gridfs.GridFS(db)

TZ = pytz.timezone('Asia/Bangkok')
UPLOAD_PATH = './uploads/'

reload(sys)
sys.setdefaultencoding('utf8')

@app.route('/db2', methods=['GET', 'POST'])
def db2():
    try:
        # conn = pymssql.connect(MSSQL_SERVER, MSSQL_USER, MSSQL_PASSWORD, MSSQL_DB)
        # cursor = conn.cursor()
        # sql = "SELECT * FROM sys.databases"
        # cursor.execute(sql)
        # data = cursor.fetchall()
        # columns = [column[0] for column in cursor.description]
        # conn.commit()
        # cursor.close()
        # results = toJson(data, columns)
        # return jsonify(results)
        return excel_col(26**3+26**2+26**4+26)
    except Exception as e:
        raise
        # return str(e)

def isAscii(s):
    return all(ord(c) < 128 for c in str(s))

def excel_col(col):
    """Covert 1-relative column number to excel-style column label."""
    quot, rem = divmod(col-1,26)
    return excel_col(quot) + chr(rem+ord('A')) if col!=0 else ''

def toJson(data,columns):
    results = []
    for row in data:
        results.append(dict(zip(columns, row)))
    return results

def toDict(data,columns):
    results = {}
    for row in data:
        results = dict(zip(columns, row))
    return results

def getNowDatetime():
    return datetime.now(TZ).strftime('%s')

def getFileExtension(filename):
    return filename.split('.').pop()

def getUserID (user):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id FROM user WHERE email=%s and firstname=%s and lastname=%s"
    cursor.execute(sql, (user['email'], user['firstname'], user['lastname']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    conn.commit()
    cursor.close()
    if len(result) != 0 :
        return str(result[0]['user_id'])
    else:
        return jsonify({"status": "Failed, not found userid"})

def getUserIDjustEmail (email):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id FROM user WHERE email=%s"
    cursor.execute(sql, (email))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    conn.commit()
    cursor.close()
    if len(result) != 0 :
        return result[0]['user_id']
    else:
        return jsonify({"status": "Failed, not found userid"})

def getLevelUser(email):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT privilege_level AS lvl FROM user WHERE email = %s"
    cursor.execute(sql, (email))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    conn.commit()
    cursor.close()

    if len(result) != 0 :
        return result[0]['lvl']
    else:
        return 'not found'

def getOwnerUIDbyUserEmail(email):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT owner_uid FROM owner_user WHERE user_uid IN (SELECT user_id FROM user WHERE email = %s) LIMIT 1"
    cursor.execute(sql, (email))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    conn.commit()
    cursor.close()

    if len(result) != 0 :
        return result[0]['owner_uid']
    else:
        return 'not found'

'''
# Include datetime version
formatter = '%Y-%m-%d %H:%M:%S'
testType = [
    # (Type, Test)
    (int, int),
    (float, float),
    (datetime, lambda value: datetime.strptime(value, formatter))
]

def getType(value):
     for typ, test in testType:
         try:
             test(value)
             return typ
         except ValueError:
             continue
     # No match
     return str
'''

def getDataType(val):
    try:
        float(val)
        if '.' in val: return 'float'
        else: return 'int'
    except ValueError:
        return 'string'
    except TypeError:
        current_app.logger.info(val)
        return None

def watprint(whatever):
    return current_app.logger.info(whatever)

# use in user management
def checkOwner(owner_id, user_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    sqlOwnerCheck = "SELECT * FROM owner_user WHERE owner_uid = %s AND user_uid = %s"
    cursor.execute(sqlOwnerCheck, (owner_id, user_id))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    conn.commit()
    cursor.close()
    return len(result)

# use in user management
def checkPreOwner(owner_id, email):
    conn = mysql.connect()
    cursor = conn.cursor()
    sqlOwnerCheck = "SELECT * FROM pre_add_owner_user WHERE owner_uid = %s AND email = %s"
    cursor.execute(sqlOwnerCheck, (owner_id, email))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    conn.commit()
    cursor.close()
    return len(result)

# check admin level for not access into mgmt page
def isAdmin (user):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT privilege_level FROM user WHERE email=%s and firstname=%s and lastname=%s"
    cursor.execute(sql, (user['email'], user['firstname'], user['lastname']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    conn.commit()
    cursor.close()
    if len(result) != 0:
        if result[0]['privilege_level'] == '1' or result[0]['privilege_level'] == '2':
            return True
        return False
    else:
        # return jsonify({"status": "Failed, not found user"})
        return False
