#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *

# =====================
# === File Uploader ===
# =====================

@app.route('/getFile/<path>/<fileName>', methods=['GET'])
def getFile(path, fileName):
    path = os.path.join('uploads/' + path)
    full_path = os.path.realpath(path)
    return send_from_directory(full_path, fileName, as_attachment=True)

# @app.route('/deleteAll', methods=['POST'])
# def deleteAll():
#     userID = getUserID(request.form['email'], request.form['firstname'], request.form['lastname'])
#     if (userID != "not found"):
#         path = 'uploads/' + userID + '/'
#         try:
#             shutil.rmtree(path)
#             return 'success'
#         except:
#             return 'fail'
#     else:
#         return jsonify({"status": "upload error, not found user's ID"})
#
# @app.route('/deleteFile', methods=['POST'])
# def deleteFile():
#     userID = getUserID(request.form['email'], request.form['firstname'], request.form['lastname'])
#     if (userID != "not found"):
#         path = 'uploads/' + userID + '/'
#         try:
#             os.remove('uploads/' + userID)
#             return 'success'
#         except:
#             return 'fail'
#     else:
#         return jsonify({"status": "upload error, not found user's ID"})

@app.route('/upload', methods=['POST'])
def upload():
    userID = getUserID(request.form['email'], request.form['firstname'], request.form['lastname'])
    if (userID != "not found"):
        path = 'uploads/' + userID + '/'
        if not os.path.exists(path):
            os.makedirs(path)

        try:
            fileHeader = request.files['fileHeader']
            fileNameHeader = fileHeader.filename
            fileHeaderType = fileNameHeader.split('.')[-1]
            fileHeader.filename = 'U' + str(userID) + '_' + request.form['source_id'] + '_Header.' + fileHeaderType

            try:
                os.remove(os.path.join(path, fileHeader.filename))
            except OSError:
                pass

            if fileHeader and allowed_file(fileHeader.filename):
                fileHeader.save(os.path.join(path, fileHeader.filename))
        except Exception as e:
            current_app.logger.info("Error in header: " + str(e))

        try:
            file = request.files['file']
            fileName = file.filename
            fileType = fileName.split('.')[-1]
            file.filename = 'U' + str(userID) + '_' + request.form['source_id'] + '.' + fileType

            try:
                os.remove(os.path.join(path, file.filename))
            except OSError:
                pass

            if file and allowed_file(file.filename):
                file.save(os.path.join(path, file.filename))
            else:
                return jsonify({"status": "file is not allowed"})
            return jsonify({"status": "success"})
        except Exception as e:
            current_app.logger.info("Error in file: " + str(e))
            return jsonify({"status": "Error in file upload"})
    else:
        return jsonify({"status": "Upload error, not found user's ID"})

@app.route('/uploadMacro', methods=['POST'])
def uploadMacro():
    userID = getUserID(request.form['email'], request.form['firstname'], request.form['lastname'])
    if (userID != "not found"):
        file = request.files['file']
        fileName = file.filename
        fileType = fileName.split('.')[-1]
        fileName = fileName.replace(fileType, '')
        path = 'uploads/' + userID + '/' + fileName + 'macro/'
        if not os.path.exists(path):
            os.makedirs(path)
        # nowDatetime = getNowDatetime()
        # newFileName =  nowDateTime + '_' + setFileName(fileName) + fileType
        # newFileName = fileName + fileType
        # file.filename = newFileName
        if file and allowed_file(file.filename):
        	file.save(os.path.join(path, file.filename))
        else:
            return jsonify({"status": "file is not allowed"})
        return jsonify({"status": "success"})
        # tempPath = userID + '/' + file.filename
        # current_app.logger.info(request.form['key'])
    else:
        return jsonify({"status": "upload error, not found user's ID"})

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

def setFileName(fileName):
    result = ''
    for st in fileName.split('.')[:-1]:
        result += st
        result += '.'
    return result

def getUserID (email, firstname, lastname):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT user_id, firstname, lastname FROM user WHERE email=%s"
    cursor.execute(sql, (email))
    data = cursor.fetchall()
    conn.commit()
    cursor.close()
    columns = [column[0] for column in cursor.description]
    result = toJson(data,columns)

    if (firstname == result[0]['firstname'] and lastname == result[0]['lastname']):
        return str(result[0]['user_id'])
    else:
        return "not found"

# @app.route('/rmu/<secret>/<userID>', methods=['GET'])
# def rmu(secret, userID):
#     hidden_secret = datetime.now(TZ).strftime('%d')
#     if secret == hidden_secret:
#         uid = str(userID)
#         path = 'uploads/' + uid + '/'
#         try:
#             conn = mysql.connect()
#             cursor = conn.cursor()
#             sql = "DELETE FROM metadataTable WHERE user_id = %s"
#             cursor.execute(sql, (uid))
#             data = cursor.fetchall()
#             conn.commit()
#             cursor.close()
#
#             fs_files = fs.find({ 'fid': {"$regex" : ".*"+uid+".*"} })
#             for value in fs_files:
#                 fs.delete(value._id)
#             shutil.rmtree(path) if os.path.exists(path) else None
#             return jsonify({"result": "success"})
#         except Exception as e:
#             return jsonify({"result": "invalid, " + str(e)})
#     else:
#         return jsonify({"result": "nice try :D"})
