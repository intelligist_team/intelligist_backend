#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dbConfig import *
import uuid

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

def sendMailConfirmRegister(token, email, link):
    # token, email, link
    # link = 'http://localhost:8080'
    # token = 'klsakdfjlksdjflkj'
    # email = 'sirawit14@gmail.com'

    fromaddr = "Intelligist Platform <chana.su@intelligist.co.th>"
    toaddr = email
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Verify Email Address for Intelligist Platform"
    link = str(link) + "/verify/" + str(token)
    footer ="<br><br><br>Team Intelligist Platform" + "<br>Call Center: +66 2 257 7000" + "<br>Email: chana.su@intelligist.co.th"
    body = "<p style='text-indent:40px;padding-left:40px;padding-right:40px;font-size: 14px;width: 410px; margin-right: auto; margin-left: auto;'>Thanks for registering for an account on Intelligist Platform<br> Before we get started, we just need to confirm that this is you.<br>Click below to verify your email address</p> <a href='" + str(link) + "' style='margin: 0 auto;display: block;width: 160px;height: 60px;margin-top: 30px;background-color: #19b5fe;text-align: center;line-height: 60px;color: #ffffff;border-radius: 4px;text-decoration: none;'>Verify Email</a>" + footer

    msg.attach(MIMEText(body, 'html',"utf-8"))
    try:
        server = smtplib.SMTP('mailtx.inet.co.th', 25)
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        return "success"
    except:
       return "error"

def sendMailWelcomeForRegister(email):
    # email = 'sirawit14@gmail.com'

    fromaddr = "Intelligist Platform <chana.su@intelligist.co.th>"
    toaddr = email
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Welcome to Intelligist Platform"
    footer ="<br><br><br>Team Intelligist Platform" + "<br>Call Center: +66 2 257 7000" + "<br>Email: chana.su@intelligist.co.th"
    body = "<p style='text-indent:40px;padding-right:40px;font-size: 14px;'>Welcome to Intelligist Platform,<br>Log in with your Email and Password to find amazing dashboard." + footer

    msg.attach(MIMEText(body, 'html',"utf-8"))
    try:
        server = smtplib.SMTP('mailtx.inet.co.th', 25)
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        return "success"
    except:
       return "error"

@app.route('/resendEmailConfirmation', methods=['POST'])
def resendEmailConfirmation():
    dataInput = request.json
    token = (str(uuid.uuid4()) + str(uuid.uuid1())).replace('-','')
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "INSERT INTO token_register VALUES (NULL, %s, %s, %s, NULL)"
    cursor.execute(sql, (token, dataInput['email'], 'active'))
    conn.commit()
    cursor.close()
    sendMailConfirmRegister(token, dataInput['email'], dataInput['link'])
    return 'success'

@app.route('/firstStepRegister', methods=['POST'])
def firstStepRegister():
    dataInput = request.json

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT * FROM user WHERE email = %s"
    cursor.execute(sql, dataInput['email'])
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    if len(data):
        if str(result[0]['status']) == '4':
            return 'Please check in your email confirmation'
        return 'already in use'

    token = (str(uuid.uuid4()) + str(uuid.uuid1())).replace('-','')
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "INSERT INTO token_register VALUES (NULL, %s, %s, %s, NULL)"
    cursor.execute(sql, (token, dataInput['email'], 'active'))
    conn.commit()
    cursor.close()

    # pre_add_role
    try:
        roleSelected = dataInput['roles']
    except Exception as e:
        roleSelected = []
    if roleSelected and len(roleSelected) > 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "SELECT * FROM pre_add_role WHERE email = %s"
        cursor.execute(sql, (dataInput['email']))
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data, columns)
        if len(result) == 0:
            for role_id in roleSelected:
                sql = "INSERT INTO pre_add_role VALUES (NULL, %s, %s, NULL)"
                cursor.execute(sql, (dataInput['email'], str(role_id)))
                conn.commit()
        else: # check dup before insert
            ResultJustRoleID = [int(obj['role_id']) for obj in result]
            cleanArray = list(set(roleSelected)-set(ResultJustRoleID))
            for role_id in cleanArray:
                sql = "INSERT INTO pre_add_role VALUES (NULL, %s, %s, NULL)"
                cursor.execute(sql, (dataInput['email'], str(role_id)))
                conn.commit()
        cursor.close()

    # pre_add_group
    try:
        groupSelected = dataInput['groups']
    except Exception as e:
        groupSelected = []
    if groupSelected and len(groupSelected) > 0:
        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "SELECT * FROM pre_add_group WHERE email = %s"
        cursor.execute(sql, (dataInput['email']))
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data, columns)
        if len(result) == 0:
            for group_id in groupSelected:
                sql = "INSERT INTO pre_add_group VALUES (NULL, %s, %s, NULL)"
                cursor.execute(sql, (dataInput['email'], str(group_id)))
                conn.commit()
        else: # check dup before insert
            ResultJustGroupID = [int(obj['group_id']) for obj in result]
            cleanArray = list(set(groupSelected)-set(ResultJustGroupID))
            for group_id in cleanArray:
                sql = "INSERT INTO pre_add_group VALUES (NULL, %s, %s, NULL)"
                cursor.execute(sql, (dataInput['email'], str(group_id)))
                conn.commit()
        cursor.close()

    # pre_add_owner_uid
    try:
        owner = dataInput['owner']
        owner_uid = getUserID(owner)
    except Exception as e:
        owner_uid = '0'
    if owner_uid != '0':
        conn = mysql.connect()
        cursor = conn.cursor()
        sql = "INSERT INTO pre_add_owner_user VALUES (NULL, %s, %s, NULL)"
        cursor.execute(sql, (dataInput['email'], str(owner_uid)))
        conn.commit()
        cursor.close()

    default_level = 3 # user level
    status = 4 # Status = Pending

    conn = mysql.connect()
    cursor = conn.cursor()
    # user_id, email, password, firstname, lastname, create_at, privilege_level, job_title, status, last_updated
    sql = "INSERT INTO user VALUES (NULL, %s, '', '', '', NULL, %s, '', %s, NULL)"
    cursor.execute(sql, (dataInput['email'], default_level, status))
    conn.commit()
    cursor.close()

    sendMailConfirmRegister(token, dataInput['email'], dataInput['link'])
    return 'success'

@app.route('/getEmailFromToken', methods=['POST'])
def getEmailFromToken():
    dataInput = request.json

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT email FROM token_register WHERE token = %s"
    cursor.execute(sql, (dataInput['token']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)

    if len(data) == 0:
        return jsonify({'status': 'not found'})

    return jsonify({'status': 'found', 'email': result[0]['email']})

@app.route('/secondStepRegister', methods=['POST'])
def secondStepRegister():
    dataInput = request.json
    default_level = 3 # user level

    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT * FROM user WHERE email = %s"
    cursor.execute(sql, (dataInput['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    conn.commit()
    cursor.close()

    if len(result) == 0:
        return 'Email not found.'

    if str(result[0]['status']) == '4': # check status is pending
        conn = mysql.connect()
        cursor = conn.cursor()

        sql = "SELECT owner_uid FROM pre_add_owner_user WHERE email = %s"
        cursor.execute(sql, (dataInput['email']))
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        result = toJson(data, columns)

        if len(result) == 0: # Guest Sign Up
            status = 5 # Status = New
        else:
            status = 1 # Status = Offline

        # user_id, email, password, firstname, lastname, create_at, privilege_level, job_title, status, last_updated
        sql = "UPDATE user SET password = %s, firstname = %s, lastname = %s, job_title = %s, status = %s WHERE email = %s"
        cursor.execute(sql,(dataInput['password'], dataInput['firstname'], dataInput['lastname'], dataInput['job_title'], status, dataInput['email']))

        sql = "UPDATE token_register SET status = %s WHERE email = %s"
        cursor.execute(sql,('inactive', dataInput['email']))
        conn.commit()
        cursor.close()

        user = { 'firstname': dataInput['firstname'], 'lastname': dataInput['lastname'], 'email': dataInput['email'] }
        addPreRoles(user)
        addPreGroups(user)
        addPreOwnerUser(user)
        removeOldPreAddOwner(dataInput['email'])
        sendMailWelcomeForRegister(dataInput['email'])
        return 'success'
    else:
        return 'Your email is duplicated.'

# After confirmation
def addPreRoles(user):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT role_id FROM pre_add_role WHERE email = %s"
    cursor.execute(sql, (user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    if len(result) != 0:
        user_id = getUserID(user)
        for row in result:
            sql = "INSERT INTO user_list_role VALUES (NULL, %s, %s)"
            cursor.execute(sql, (row['role_id'], str(user_id)))
            conn.commit()
    cursor.close()

# After confirmation
def addPreGroups(user):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT group_id FROM pre_add_group WHERE email = %s"
    cursor.execute(sql, (user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    if len(result) != 0:
        user_id = getUserID(user)
        for row in result:
            sql = "INSERT INTO user_list_group VALUES (NULL, %s, %s)"
            cursor.execute(sql, (row['group_id'], str(user_id)))
            conn.commit()
    cursor.close()

# After confirmation
def addPreOwnerUser(user):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "SELECT owner_uid FROM pre_add_owner_user WHERE email = %s"
    cursor.execute(sql, (user['email']))
    data = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    result = toJson(data, columns)
    user_id = getUserID(user)

    if len(result) != 0:
        sql = "SELECT id FROM owner_user WHERE owner_uid = %s AND user_uid = %s"
        cursor.execute(sql, (str(result[0]['owner_uid']), str(user_id)))
        data = cursor.fetchall()
        columns = [column[0] for column in cursor.description]
        ownerResult = toJson(data, columns)

        if len(ownerResult) == 0:
            sql = "INSERT INTO owner_user VALUES (NULL, %s, %s)"
            cursor.execute(sql, (str(result[0]['owner_uid']), str(user_id)))
            conn.commit()

        userStatus = 1 # status Offline
        sql = "UPDATE user SET status = %s WHERE user_id = %s"
        cursor.execute(sql, (userStatus, str(user_id)))
        conn.commit()
    else:
        userStatus = 5 # status NEW (waiting for root assigned)
        sql = "UPDATE user SET status = %s WHERE user_id = %s"
        cursor.execute(sql, (userStatus, str(user_id)))
        conn.commit()
    cursor.close()

def removeOldPreAddOwner(email):
    conn = mysql.connect()
    cursor = conn.cursor()
    sql = "DELETE FROM pre_add_owner_user WHERE email = %s"
    result = cursor.execute(sql, email)
    conn.commit()
    cursor.close()

'''
# IDEA: อาจจะทำ function เพิ่ม ในการเช็คว่า token (path) เป็น inactive ไหม ถ้าเป้นก็ return false ให้ frontend redirect '/' ไป เพื่อเพิ่ม security ได้บ้าง
ในกรณีที่ user เข้า link token path เดิม จะได้เข้าไม่ได้)
'''
